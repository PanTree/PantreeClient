# Build
clone repo, cd to repo directory

mkdir build

cd build

cmake ..

make -j4

*optional: will deploy it to ../install*: make install 

# Update <Stephen Hayes> : 10/29/2018
Same as Sprint 5 -- implement functional managers/models for Lists and Recipes. A list will represent 0 to n items. A recipe will represent 0 to n inventory items + 0 to n directions. We ought to have prototype managers in place prior to API and algorithm testing.

# Update <Stephen Hayes> : 10/27/2018
Sprint 5 ends tomorrow 6 pm.  Push your code asap so we have time to check for merge issues and basic integration.  I don't want to write this entire project; I may start sending the prof snapshots of activity from our repo.
If C++ isn't your jam, then talk to me in private message and we'll figure out some alternative tickets... e.g., testing, ui...

# Update <Stephen Hayes> : 10/20/2018
Dialog boxes added for user stuff.  Messy uncommented code right now, but the architecture is starting to come together.  We'll be able to add the other models/user input fields easy enough. 

# Update: 10/14/2018
Making progress.  Once core features are in, we can start cleaning up the UI.  The models read their schema from .json files so adding new columns will be easy.

# README.md
# 1) Pre-reqs
## a) Qt5
Qt5 is required.  Qt 5.11 is recommended.  There _shouldn't_ be any issues with older variations of Qt5... but, please just use 5.11.  If we need backwards compatability, we'll do some tests. 
## b) cmake 3.0
Cmake is decent about backwards compatability; but if you're having build issues, try and get the newest version (3.11.x).
## c) C++11
C++11 at a minimum -- 14 or 17 reccomended.