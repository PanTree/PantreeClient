﻿//
// Created by Stephen Hayes on 10/02/18.
//

#include "src/core/networking/TcpClient.h"

#include <QtWidgets>
#include <QtNetwork>
#include <src/core/parser/FileParser.h>

namespace core
{
namespace networking
{

TcpClient::TcpClient(QWidget *parent)
    :QWidget(parent),
      m_hostCombo(new QComboBox(this)),
      m_portLineEdit(new QLineEdit(this)),
      m_tcpSocket(new QTcpSocket(this))
{
    setObjectName("TcpClient");
    setUI();
    setClient();

    in.setDevice(m_tcpSocket);
    in.setVersion(QDataStream::Qt_5_0);

    connect(m_tcpSocket, &QIODevice::readyRead, this, &TcpClient::readData);
    connect(m_tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &TcpClient::displayError);
}

void TcpClient::setUI()
{
    m_hostCombo->setEditable(true);

    auto hostLabel = new QLabel("&Server name:");
    hostLabel->setBuddy(m_hostCombo);
    m_hostCombo->setCurrentText("192.168.1.169");

    auto portLabel = new QLabel("Port: ");
    portLabel->setBuddy(m_portLineEdit);
    m_portLineEdit->setText("45000");

    auto bytesSentLabel = new QLabel("Bytes Sent: ");
    auto bytesReceivedLabel = new QLabel("Bytes Received: ");
    m_bytesSent = new QLabel("0");
    m_bytesReceived = new QLabel("0");

    auto mainLayout = new QGridLayout(this);

    mainLayout->addWidget(hostLabel, 0, 0);
    mainLayout->addWidget(m_hostCombo, 0, 1);
    mainLayout->addWidget(portLabel, 1, 0);
    mainLayout->addWidget(m_portLineEdit, 1, 1);
    mainLayout->addWidget(bytesSentLabel, 6, 0, 1, 1);
    mainLayout->addWidget(bytesReceivedLabel, 7, 0, 1, 1);
    mainLayout->addWidget(m_bytesSent, 6, 1, 1, 1);
    mainLayout->addWidget(m_bytesReceived, 7, 1, 1, 1);

    m_userDisplay = new QPlainTextEdit(this);
    mainLayout->addWidget(m_userDisplay, 8, 0, 6, 6);

    auto connectButton = new QPushButton("Connect");
    mainLayout->addWidget(connectButton, 14, 0, 1, 1);
    connect(connectButton, &QAbstractButton::pressed, this, &TcpClient::establishConnection);

    auto disconnectButton = new QPushButton("Disconnect");
    mainLayout->addWidget(disconnectButton, 14, 1, 1, 1);
    connect(disconnectButton, &QAbstractButton::pressed, this, &TcpClient::closeConnection);
}

void TcpClient::setClient()
{
    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired)
    {
        const auto &config = manager.defaultConfiguration();
        m_networkSession = new QNetworkSession(config, this);
        m_networkSession->open();
    }
}

bool TcpClient::establishConnection()
{
    qDebug() << "begin.";
    m_userDisplay->appendPlainText("Attempting to connect...");
    m_tcpSocket->connectToHost(m_hostCombo->currentText(), m_portLineEdit->text().toInt());
    return true;
}

void TcpClient::closeConnection()
{
    qDebug() << "closeConnection begin.";
    m_userDisplay->appendPlainText("Closing connection.");
    m_tcpSocket->abort();
    qDebug() << "closeConnection disconnected from server.";
}

void TcpClient::readData()
{
    qDebug() << "readData begin.";
    in.startTransaction();
    QByteArray ba;
    in >> ba;
    if (!in.commitTransaction())
    {
        qDebug() << "::readData !in.commitTransaction().";
        return;
    }
    QString s = QString::fromUtf8(ba);
    int bytes_received = m_bytesReceived->text().toInt();
    bytes_received += s.size();
    m_bytesReceived->setText(QString::number(bytes_received));
    parseResponse(s);
}

void TcpClient::parseResponse(const QString &response)
{
    qDebug() << objectName() + "::parseReponse begin, (response): " + response;
    m_userDisplay->appendPlainText(response);
}

void TcpClient::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError)
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The host was not found. Please check the "
                                    "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                 tr("The connection was refused by the peer. "
                                    "Make sure the server is running, "
                                    "and check that the host name and port "
                                    "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                 tr("The following error occurred: %1.")
                                 .arg(m_tcpSocket->errorString()));
    }
}

void TcpClient::createUserSlot(const QString &username, const QString &password)
{
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::ReadWrite);
    ds.setVersion(QDataStream::Qt_5_0);
    QString s{username + ":" + password};
    ds << s.toUtf8();
    qDebug() << s;
    m_tcpSocket->write(ba);
}

}
}
