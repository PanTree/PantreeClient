﻿//
// Created by Stephen Hayes on 10/02/18.
//
#pragma once

#include <QDataStream>
#include <QTcpSocket>
#include <QWidget>

class QComboBox;
class QLabel;
class QLineEdit;
class QNetworkSession;
class QPlainTextEdit;
class QPushButton;
class QTcpSocket;

namespace core
{
namespace networking
{

class TcpClient : public QWidget
{
Q_OBJECT

public:

    TcpClient(QWidget *parent);
    void createUserSlot(const QString &username, const QString &password);
    bool connected() const {return m_connected;}
    bool establishConnection();

signals:

    void logSignal(const QString &data);

private:

    void parseResponse(const QString &response);

    QComboBox *m_hostCombo = nullptr;
    QDataStream in;
    QLabel *m_bytesSent = nullptr;
    QLabel *m_bytesReceived = nullptr;
    QLineEdit *m_portLineEdit = nullptr;
    QPlainTextEdit *m_userDisplay = nullptr;
    QNetworkSession *m_networkSession = nullptr;
    QTcpSocket *m_tcpSocket = nullptr;
    bool m_connected = false;

private slots:

    void setClient();
    void setUI();
    void closeConnection();
    void displayError(QAbstractSocket::SocketError socketError);
    void readData();
};

}
}
