﻿//
// Created by Stephen Hayes on 9/26/18.
//

#include "src/core/mainwindow.h"
#include "src/core/manager/AccountManager.h"
#include "src/core/manager/InventoryManager.h"
#include "src/core/manager/NetworkManager.h"
#include "src/core/manager/ListManager.h"
#include "src/core/manager/RecipeManager.h"
#include "src/core/manager/OptionsManager.h"

#include <QtWidgets>

namespace core
{
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_centralWidget(new QStackedWidget(this))
{
    setCentralWidget(m_centralWidget);
    setObjectName("MainWindow");
    setWindowTitle("Pantree Client v0.9.2");
    setMenu();
    setToolBar();
    setLog();
    initAccountManager();
}

void MainWindow::setToolBar()
{
    m_toolBar = new QToolBar("Tools");
    addToolBar(m_toolBar);

    auto userManagerAction = new QAction(QIcon(":/icons/Account.png"), "User Manager", this);
    userManagerAction->setData(0);
    connect(userManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(userManagerAction->data().toInt());
    });

    auto networkManagerAction = new QAction(QIcon(":/icons/Network.png"), "Network Manager", this);
    networkManagerAction->setData(1);
    connect(networkManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(networkManagerAction->data().toInt());
    });

    auto inventoryManagerAction = new QAction(QIcon(":/icons/Inventory.png"), "Inventory Manager", this);
    inventoryManagerAction->setData(2);
    connect(inventoryManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(inventoryManagerAction->data().toInt());
    });

    auto recipeManagerAction = new QAction(QIcon(":/icons/Recipe.png"), "Recipe Manager", this);
    recipeManagerAction->setEnabled(true);
    recipeManagerAction->setData(3);
    connect(recipeManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(recipeManagerAction->data().toInt());
    });

    auto listManagerAction = new QAction(QIcon(":/icons/List.png"), "List Manager", this);
    listManagerAction->setData(4);
    connect(listManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(listManagerAction->data().toInt());
    });

    auto optionManagerAction = new QAction(QIcon(":/icons/ControlBox.png"), "Option Manager", this);
    optionManagerAction->setData(5);
    connect(optionManagerAction, &QAction::triggered, this, [=]()
    {
        m_centralWidget->setCurrentIndex(optionManagerAction->data().toInt());
    });

    QWidget *spacerWidget = new QWidget(this);
    spacerWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    spacerWidget->setVisible(true);

    m_userActionGroup = new QActionGroup(this);
    m_userActionGroup->addAction(inventoryManagerAction);
    m_userActionGroup->addAction(listManagerAction);
    m_userActionGroup->addAction(networkManagerAction);
    m_userActionGroup->addAction(recipeManagerAction);
    m_userActionGroup->setDisabled(true);
    m_toolBar->insertAction(0, userManagerAction);
    m_toolBar->insertAction(0, networkManagerAction);
    m_toolBar->addSeparator();
    m_toolBar->insertAction(0, inventoryManagerAction);
    m_toolBar->insertAction(0, recipeManagerAction);
    m_toolBar->insertAction(0, listManagerAction);
    m_toolBar->addWidget(spacerWidget);
    m_toolBar->insertAction(0, optionManagerAction);
}

void MainWindow::setLog()
{
    m_configLog = new QTextEdit(this);
    auto logLayout = new QVBoxLayout;
    logLayout->addWidget(m_configLog);
    auto dialog = new QDialog(this);
    dialog->setWindowTitle("Debug Log");
    dialog->setLayout(logLayout);
    dialog->show();
}

void MainWindow::setMenu()
{
    auto aboutAct = new QAction("About", this);
    aboutAct->setStatusTip("Show the application's About box");
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

    auto quitAct = new QAction("Quit", this);
    quitAct->setStatusTip("Quit Application");
    connect(quitAct, &QAction::triggered, this, &MainWindow::close);

    auto fileMenu = menuBar()->addMenu("File");
    fileMenu->addSeparator();
    fileMenu->addAction(quitAct);

    auto helpMenu = menuBar()->addMenu("Help");
    helpMenu->addAction(aboutAct);
    m_activeUser = new QLabel(this);
    auto activeUserLabel = new QLabel("Logged in as: ", this);
    statusBar()->addWidget(activeUserLabel);
    statusBar()->addWidget(m_activeUser);
}

void MainWindow::initAccountManager()
{
    const QString &logString = "attempting to instantiate the AccountManager.";
    qDebug() << logString;
    logSlot(objectName(), logString);

    m_accountManager=new manager::AccountManager(this);
    connect(m_accountManager, &manager::BaseManager::logSignal, this, &MainWindow::logSlot);
    connect(m_accountManager, &manager::AccountManager::loginSignal, this, &MainWindow::loginSlot);
    connect(m_accountManager, &manager::AccountManager::logoutSignal, this, &MainWindow::logoutSlot);
    connect(this, &MainWindow::loggedInSignal, m_accountManager, &manager::AccountManager::loggedInSlot);
    connect(this, &MainWindow::loggedOutSignal, m_accountManager, &manager::AccountManager::loggedOutSlot);

    m_accountManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_accountManager));
}

void MainWindow::initModules()
{
    if(initNetworkManager())
        logSlot(objectName(), "Network Manager Loaded.", LogPriority::PositiveAlert);

    if(initInventoryManager())
        logSlot(objectName(), "Inventory Manager Loaded.", LogPriority::PositiveAlert);

    if(initRecipeManager())
        logSlot(objectName(), "Recipe Manager Loaded.", LogPriority::PositiveAlert);

    if(initListManager())
        logSlot(objectName(), "List Manager Loaded.", LogPriority::PositiveAlert);

    if(initOptionManager())
        logSlot(objectName(), "Options Manager Loaded.", LogPriority::PositiveAlert);

    m_userActionGroup->setEnabled(true);
    connect(m_listManager, &manager::ListManager::checkInventorySignal, m_inventoryManager, &manager::InventoryManager::checkInventorySlot);
    connect(m_listManager, &manager::ListManager::getRecipeSignal, m_recipeManager, &manager::RecipeManager::getRecipesSlot);
    connect(m_listManager, &manager::ListManager::getRecipeByNameSignal, m_recipeManager, &manager::RecipeManager::getRecipeByNameSlot);
}

void MainWindow::loginSlot(const QString &username)
{
    m_userName = username;
    initModules();
    m_loggedIn = true;
    m_activeUser->setText(username);
    logSlot(objectName(), + ":: "+ m_userName + "logged in.", LogPriority::PositiveAlert);
    m_userActionGroup->setEnabled(true);
    emit loggedInSignal();
}

void MainWindow::logoutSlot()
{
    m_inventoryManager->deleteLater();
    logSlot(objectName(), + ":: "+ m_userName + " Inventory Manager shutting down.", LogPriority::PositiveAlert);

    m_networkManager->deleteLater();
    logSlot(objectName(), + ":: "+ m_userName + " Network Manager shutting down.", LogPriority::PositiveAlert);

    m_listManager->deleteLater();
    logSlot(objectName(), + ":: "+ m_userName + " List Manager shutting down.", LogPriority::PositiveAlert);

    m_recipeManager->deleteLater();
    logSlot(objectName(), + ":: "+ m_userName + " Recipe Manager shutting down.", LogPriority::PositiveAlert);

    m_optionManager->deleteLater();
    logSlot(objectName(), + ":: "+ m_userName + " Option Manager shutting down.", LogPriority::PositiveAlert);

    m_loggedIn = false;
    m_activeUser->clear();
    logSlot(objectName(), + ":: "+ m_userName + " logged out.", LogPriority::PositiveAlert);
    m_userName = QString();
    m_userActionGroup->setEnabled(false);
    emit loggedOutSignal();
}

bool MainWindow::initInventoryManager()
{
    const QString &logString = "attempting to instantiate InventoryManager.";
    qDebug() << logString;
    logSlot(objectName(), logString);

    m_inventoryManager = new manager::InventoryManager(m_userName, this);
    connect(m_inventoryManager, &manager::InventoryManager::logSignal, this, &MainWindow::logSlot);
    connect(m_inventoryManager, &manager::AccountManager::destroyed, [&](){logSlot(objectName(), "Inventory Manager successfully destroyed.", LogPriority::PositiveAlert);});
    m_inventoryManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_inventoryManager));
    return true;
}

bool MainWindow::initRecipeManager()
{
    const QString &logString = "attempting to instantiate RecipeManager.";
    qDebug() << logString;
    logSlot(objectName(), logString);

    m_recipeManager = new manager::RecipeManager(m_userName, this);
    connect(m_recipeManager, &manager::RecipeManager::logSignal, this, &MainWindow::logSlot);
    connect(m_recipeManager, &manager::RecipeManager::destroyed, [&](){logSlot(objectName(), "Recipe Manager successfully destroyed.", LogPriority::PositiveAlert);});
    m_recipeManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_recipeManager));
    return true;
}

bool MainWindow::initListManager()
{
    const QString &logString = "attempting to instantiate ListManager.";
    qDebug() << logString;
    logSlot(objectName(), logString);

    m_listManager = new manager::ListManager(m_userName, this);
    connect(m_listManager, &manager::ListManager::logSignal, this, &MainWindow::logSlot);
    connect(m_listManager, &manager::ListManager::destroyed, [&](){logSlot(objectName(), "List Manager successfully destroyed.", LogPriority::PositiveAlert);});
    m_listManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_listManager));
    return true;
}

bool MainWindow::initNetworkManager()
{
    const QString &logString = "attempting to instantiate NetworkManager.";
    qDebug() << logString;
    logSlot(objectName(), logString);

    m_networkManager=new manager::NetworkManager(m_userName, this);
    connect(m_networkManager, &manager::BaseManager::logSignal, this, &MainWindow::logSlot);
    connect(m_networkManager, &manager::NetworkManager::destroyed, [&](){logSlot(objectName(), "Network Manager successfully destroyed.", LogPriority::PositiveAlert);});
    m_networkManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_networkManager));
    return true;
}

bool MainWindow::initOptionManager()
{
    qDebug() <<  "attempting to instantiate OptionsManager.";
    m_optionManager = new manager::OptionManager(m_userName, this);
    connect(m_optionManager, &manager::OptionManager::logSignal, this, &MainWindow::logSlot);
    connect(m_optionManager, &manager::OptionManager::destroyed, [&](){logSlot(objectName(), "Option Manager sucessfully destroyed.", LogPriority::PositiveAlert);});
    m_optionManager->init();
    m_centralWidget->addWidget(qobject_cast<QWidget *>(m_optionManager));
    return true;
}

void MainWindow::logSlot(const QString &objectName, const QString &msg, const LogPriority &priority) const
{
    switch(priority)
    {
    case LogPriority::Normal:
        m_configLog->setTextColor(Qt::black);
        break;
    case LogPriority::Warning:
        m_configLog->setTextColor(Qt::darkYellow);
        break;
    case LogPriority::Critical:
        m_configLog->setTextColor(Qt::red);
        break;
    case LogPriority::PositiveAlert:
        m_configLog->setTextColor(Qt::darkGreen);
    }
    m_configLog->append(objectName + msg);
}

void MainWindow::about()
{
    QMessageBox::about(this, ("About Menu"), ("Pantree Client Version 0.9.2"));
}
}
