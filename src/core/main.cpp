﻿#include <QApplication>
#include <QtCore>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qSetMessagePattern("[%{time yyyyMMdd h:mm:ss.z} "
                       "%{if-debug}D%{endif}"
                       "%{if-info}I%{endif}"
                       "%{if-warning}W%{endif}"
                       "%{if-critical}C%{endif}"
                       "%{if-fatal}F%{endif}] "
                       "%{function}:%{line} - %{message}");

    core::MainWindow mainWin;
    mainWin.show();
    return app.exec();
}
