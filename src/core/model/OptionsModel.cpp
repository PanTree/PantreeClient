//
//Created by Cedrik Acosta.
//

#include "src/core/model/OptionsModel.h"
#include <QDebug>

namespace core {

namespace model{

OptionModel::OptionModel(const QString &username, QObject *parent)
    :BaseModel(parent),
      m_userName(username)
{ }

void OptionModel::loadCustomConfig()
{
    m_configMap.insert("username", m_userName);
    qDebug() << objectName() + "::loadCustomConfig inserted (username): " + m_userName;
}
}
}
