﻿//
// Created by Stephen Hayes on 9/28/18.
//

#include "src/core/parser/FileParser.h"
#include "src/core/model/InventoryModel.h"
#include <QDebug>

namespace core
{

namespace model
{

InventoryModel::InventoryModel(const QString &username, QObject *parent)
:BaseModel(parent),
  m_userName(username)
{}

void InventoryModel::loadCustomConfig()
{
    m_configMap.insert("username", m_userName);
    qDebug() << objectName() + "::loadCustomConfig inserted (username): " + m_userName;
}

void InventoryModel::checkIfContains(QVariantList &requirements)
{
    /// validate requirement list; first entry is recipe title
    /// followed by item label, item quantity, and unit type

    qDebug() << "Checking inventory for recipe: " << requirements.at(0).toString();
    if((requirements.size() - 1) %3 != 0)
    {
        qCritical() << "Failure: requirements format incorrect.";
        return;
    }
    int requirementQuantity = (requirements.size() - 1)/3;
    qDebug() << "Total number of requirements: " << QString::number(requirementQuantity);
    for(int i = 1; i<requirements.size()-1; i+=3)
    {
        auto label = requirements.at(i).toString();
        auto requiredQuantity = requirements.at(i+1).toDouble();
        auto unitType = requirements.at(i+2).toString();
        qDebug() << "Label: " + label;
        qDebug() << "Required Quantity: " + QString::number(requiredQuantity);
        qDebug() << "Unit Type: " + unitType;

        /// check model for item label

        QModelIndex labelIndex = index(0,0);
        auto labelMatch = match(labelIndex, Qt::DisplayRole, label);

        if(!labelMatch.isEmpty())
        {
            qDebug() << "Found it.";
            int row = labelMatch.at(0).row();
            QModelIndex labelIndex = index(row, 0);
            QModelIndex quantityIndex = index(row, 1);
            QModelIndex unitTypeIndex = index(row, 2);
            auto currentLabel = data(labelIndex).toString();
            auto currentQuantity = data(quantityIndex).toDouble();
            auto currentType = data(unitTypeIndex).toString();
            qDebug() << "(label): " + currentLabel + " (currentQuantity): " + QString::number(currentQuantity) + " (unitType): " + currentType;
            if(currentType != unitType)
            {
                qDebug() << "Type mismatch: attempting to convert.";
                /// TODO
            }
            if(currentQuantity >= requiredQuantity)
            {
                requirements[i+1] = 0.0;
            }
            else
            {
                requirements[i+1] = (requiredQuantity - currentQuantity);
            }
        }
    }
}

}
}
