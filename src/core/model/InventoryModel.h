﻿//
// Created by Stephen Hayes on 9/28/18.
//

#pragma once

#include "BaseModel.h"

namespace core{

namespace model{

class InventoryModel : public BaseModel
{
Q_OBJECT
Q_DISABLE_COPY(InventoryModel)

public:

    explicit InventoryModel(const QString &username, QObject *parent);

public slots:

    void checkIfContains(QVariantList &requirements);

protected:

    virtual void loadCustomConfig() override;


private:

    QString m_userName;

private slots:

};

}
}
