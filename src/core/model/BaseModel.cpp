﻿//
// Created by Stephen Hayes on 9/26/18.
//

#include "src/core/model/BaseModel.h"
#include "src/core/parser/FileParser.h"

#include <QDebug>
#include <QFile>
#include <QStringList>

namespace core
{
namespace model
{

BaseModel::BaseModel(QObject *parent)
        : QAbstractTableModel(parent)
{
}

void BaseModel::init()
{
    QString name(metaObject()->className());
    QString trueName(name.section("::", -1));
    setObjectName(trueName);
    qDebug() << objectName() + "::onInit() name assignment: " + trueName;
    parseConfig();
    loadCustomConfig();
    loadData();
    emit initStatusSignal(objectName(), "Loaded");
}

void BaseModel::parseConfig()
{
    qDebug() << objectName() + "::parseConfig beginning.";
    QString configString(":" + objectName() + ".json");
    qDebug() << objectName() + "::parseConfig (configString): " + configString;

    m_configMap = utility::FileParser::parseJson(configString);
    qDebug() << objectName() + "::parseConfig (configMap.size): " + QString::number(m_configMap.size());

    if(m_configMap.contains("error"))
    {
        emit logSignal(objectName(), "::parseConfig config parse error.", LogPriority::Critical);
        qDebug() << objectName() + "::parseConfig config parse error.";
    }
    else
    {
        emit logSignal(objectName(), "::parseConfig config parsed.", LogPriority::Normal);
        qDebug() << objectName() + "::parseConfig config parsed.";
    }
}

void BaseModel::loadData()
{
    QString directory;
    QString filename;
    if(m_configMap.contains("directory"))
    {
        directory = m_configMap.find("directory").value().toString();
        if(directory == "user")
        {
            const auto &username = m_configMap.constFind("username").value().toString();
            directory.append("/");
            directory.append(username);
            directory.append("/");
        }
    }

    if(m_configMap.contains("filename"))
        filename = m_configMap.constFind("filename").value().toString();
    QString datapath{directory + filename};
    m_configMap.insert("datapath", datapath);
    qDebug() << objectName() + "::loadData loading (datapath): " + datapath;
    if(QFile(datapath).exists())
    {
        const auto &result = utility::FileParser::readTable(datapath);
        processLoadData(result);
        emit logSignal(objectName(), "loadData loaded.", core::LogPriority::PositiveAlert);
        qDebug() << objectName() + "::loadData data found.";
    }
    else
    {
        emit logSignal(objectName(), "loadData no data found.", core::LogPriority::Critical);
        qDebug() << objectName() + "::loadData no data found.";
    }
}

Qt::ItemFlags BaseModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Q_NULLPTR;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

int BaseModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_modelData.size();
}

int BaseModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_configMap.value("headercount").toInt();
}

QVariant BaseModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || role != Qt::DisplayRole)
    {
        return QVariant();
    }
    const auto &headerList = m_modelData.at(index.row());
    return headerList.at(index.column());
}

bool BaseModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    qDebug() << objectName() + "::setData begin.";
    if(index.isValid() && role == Qt::EditRole)
    {
        m_modelData[index.row()][index.column()] = value;
        processWriteData();
        emit dataChanged(index, index);
        return true;
    }
    qWarning() << objectName() + "::setData invalid index.";
    return false;
}

QVariant BaseModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            QString header("header" + QString::number(section));
            return m_configMap.value(header);
        }
    }
    return QVariant();
}

void BaseModel::processAddItem(const QVariantList &data)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_modelData.append(data);
    endInsertRows();
    emit logSignal(objectName(), "processWriteData row added.");
    processWriteData();
}

void BaseModel::processRemoveItem(const QModelIndex &index)
{
    qDebug() << "processRemoveItem attempting to remove (ROW): " + QString::number(index.row());
    qDebug() << m_modelData;
    if(index.row() < rowCount() && index.row() >= 0)
    {
        beginRemoveRows(QModelIndex(), index.row(), index.row());
        m_modelData.remove(index.row());
        emit logSignal(objectName(), "processWriteData row removed.");
        endRemoveRows();
        processWriteData();
    }
    else
    {
        qWarning() << objectName() + "::removeItem bad index passed.";
        emit logSignal(objectName(), "processWriteData saved.", core::LogPriority::Warning);
    }
}

void BaseModel::processWriteData() const
{
    if(m_configMap.contains("datapath"))
    {
        const auto &datapath = m_configMap.constFind("datapath").value().toString();
        qDebug() << objectName() + "::processWriteData attempting to write data to (filepath): " + datapath;
        utility::FileParser::writeTable(datapath, m_modelData);
        emit logSignal(objectName(), "processWriteData saved.", core::LogPriority::PositiveAlert);
    }
    else
    {
        emit logSignal(objectName(), "processWriteData failure.", core::LogPriority::Critical);
    }
}

void BaseModel::processClearData()
{
    beginResetModel();
    m_modelData.clear();
    endResetModel();
    processWriteData();
    emit logSignal(objectName(), "processClearData data erased.", core::LogPriority::PositiveAlert);
}

void BaseModel::processLoadData(const QVector<QVariantList> &data)
{
    qDebug() << objectName() + "::processLoadData (data size): " + QString::number(data.size());
    beginResetModel();
    m_modelData.clear();
    m_modelData = data;
    endResetModel();
    emit logSignal(objectName(), "processLoadData data loaded.", core::LogPriority::PositiveAlert);
}

}
}
