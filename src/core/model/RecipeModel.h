﻿//
// Created by Ryan Dill on 10/28/18.
//

#pragma once

#include "BaseModel.h"

namespace core{
namespace model{

class RecipeModel : public BaseModel
{
Q_OBJECT
Q_DISABLE_COPY(RecipeModel)

public:

    explicit RecipeModel(const QString &username, QObject *parent);
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:

    virtual void loadCustomConfig() override;

protected slots:
private:

    const QString &m_userName;

private slots:

};

}
}
