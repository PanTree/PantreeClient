﻿//
// Created by Stephen Hayes on 10/13/18.
//

#pragma once

#include "BaseModel.h"

namespace core{

namespace model{

class UserModel : public BaseModel
{
Q_OBJECT
Q_DISABLE_COPY(UserModel)

public:

    explicit UserModel(QObject *parent);
    bool userLogin(const QString &username, const QString &password);

protected:

    virtual void loadCustomConfig() override;

};

}
}
