﻿//
// Created by tdangol on 10/24/18.
//

#include "src/core/model/ListModel.h"
#include <QDebug>

namespace core
{
namespace model
{

ListModel::ListModel(const QString &username, QObject *parent)
    :BaseModel(parent),
      m_userName(username)
{}

void ListModel::loadCustomConfig()
{
    m_configMap.insert("username", m_userName);
    qDebug() << objectName() + "::loadCustomConfig inserted (username): " + m_userName;
}

Qt::ItemFlags ListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Q_NULLPTR;

    return QAbstractItemModel::flags(index);
}

void ListModel::getLists(QVariantList &lists) const
{
    for(const auto &it : m_modelData)
    {
        lists.append(it);
    }
}

QVariant ListModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }
    if(role == Qt::DisplayRole)
    {
        int col = index.column();
        const auto &datalist = m_modelData.at(index.row());
        switch(col)
        {
            case 0: return datalist.at(0);
            case 1: return (datalist.size() / 3);
        }
    }
    else if(role == Qt::UserRole)
    {
        return m_modelData.at(index.row());
    }
    return QVariant();
}

}
}
