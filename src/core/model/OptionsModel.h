//
//Created by Cedrik Acosta.
//

#pragma once

#include "BaseModel.h"

namespace core {

namespace model {

class OptionModel: public BaseModel
{
    Q_OBJECT
    Q_DISABLE_COPY(OptionModel)

public:

    explicit OptionModel(const QString &username, QObject *parent);

protected:

    virtual void loadCustomConfig() override;

private:

    const QString &m_userName;

private slots:

};
}
}
