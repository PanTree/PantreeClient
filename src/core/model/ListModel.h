﻿//
// Created by tdangol on 10/24/18.
//

#pragma once

#include "BaseModel.h"

namespace core{

namespace model{

struct shopping_list {
        QString label;
        int id;
};

class ListModel : public BaseModel
{
    Q_OBJECT
    Q_DISABLE_COPY(ListModel)

public:

    explicit ListModel(const QString &username, QObject *parent);
    void getLists(QVariantList &lists) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
signals:
protected:

    virtual void loadCustomConfig() override;

protected slots:

private:

    const QString &m_userName;

private slots:

};

}
}
