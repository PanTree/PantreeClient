﻿//
// Created by Ryan Dill on 10/28/18.
//

#include "src/core/model/RecipeModel.h"
#include <QDebug>

namespace core
{

namespace model
{

RecipeModel::RecipeModel(const QString &username, QObject *parent)
    :BaseModel(parent),
      m_userName(username)
{}

void RecipeModel::loadCustomConfig()
{
    m_configMap.insert("username", m_userName);
    qDebug() << objectName() + "::loadCustomConfig inserted (username): " + m_userName;
}

QVariant RecipeModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }
    if(role == Qt::DisplayRole)
    {
        int col = index.column();
        const auto &datalist = m_modelData.at(index.row());
        switch(col)
        {
            case 0: return datalist.at(0);
        }
    }
    else if(role == Qt::UserRole)
    {
        return m_modelData.at(index.row());
    }
    return QVariant();
}

}
}
