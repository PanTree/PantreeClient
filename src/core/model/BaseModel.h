﻿//
// Created by Stephen Hayes on 9/26/18.
//

#pragma once

#include "src/core/enums/Enums.h"
#include "src/core/parser/FileParser.h"

#include <QAbstractTableModel>
#include <QVariantMap>

class QLabel;

namespace core {
namespace model {

class BaseModel : public QAbstractTableModel
{
Q_OBJECT
Q_DISABLE_COPY(BaseModel)

public:

    explicit BaseModel(QObject *parent);

    void init();

    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

public slots:

    virtual void processAddItem(const QVariantList &data);
    virtual void processRemoveItem(const QModelIndex &index);
    virtual void processClearData();
    virtual void processLoadData(const QVector<QVariantList> &data);
    virtual void processWriteData() const;

signals:

    void initStatusSignal(const QString &name, const QString &status);
    void logSignal(const QString &objectName, const QString &msg, const core::LogPriority &priority = LogPriority::Normal) const;

protected:

    virtual void loadData();
    virtual void loadCustomConfig() {}

    QVector<QVariantList> m_modelData;
    QVariantMap m_configMap;

protected slots:

private:

    void parseConfig();

private slots:

};


}
}
