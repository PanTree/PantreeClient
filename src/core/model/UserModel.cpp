﻿//
// Created by Stephen Hayes on 10/13/18.
//
// NOTE: In production code, we would use a well-vetted 3rd party
// library for security.  This is simplified for prototyping.

#include "src/core/parser/FileParser.h"
#include "src/core/model/UserModel.h"
#include <QDir>
#include <QDebug>
#include <QCryptographicHash>

namespace core
{
namespace model
{

UserModel::UserModel(QObject *parent)
:BaseModel(parent)
{}

void UserModel::loadCustomConfig()
{
    qDebug() << objectName() + "::loadCustomConfig begin.";
    const auto &filename = m_configMap.find("filename").value().toString();
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << objectName() + "::loadCustomConfig failure opening (filename): " + filename;
        emit logSignal(objectName(), "Failure opening " + filename, LogPriority::Critical);
    }
    else
    {
        qDebug() << objectName() + "::loadCustomConfig opened (filename): " + filename;
        emit logSignal(objectName(), "opened " + filename);
    }
}

bool UserModel::userLogin(const QString &username, const QString &password)
{
    qDebug() << objectName() + "::userLogin begin, (username): " + username + " (pw): " + password;
    const auto &userNameColumnIndex = createIndex(0, 0);
    const auto &targetRow = match(userNameColumnIndex, Qt::DisplayRole, username);
    if(targetRow.isEmpty())
    {
        qDebug() << objectName() + "::userLogin could not find (username): " + username;
        return false;
    }
    const auto &row = targetRow.at(0).row();
    const auto &targetIndex = createIndex(row, 1);
    QByteArray hashedPassword = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Md5);
    QByteArray hexPassword = hashedPassword.toHex();
    qDebug() << objectName() + "::userLogin hexPassword (data): " + QString::fromUtf8(hexPassword);
    const auto &retrievedPassword = targetIndex.data().toByteArray();
    qDebug() << objectName() + "::userLogin retrieved (data): " + QString::fromUtf8(retrievedPassword);
    if(qstrcmp(retrievedPassword, hexPassword) != 0)
    {
        qDebug() << objectName() + "::userLogin could not match (password): " + QString::fromUtf8(hexPassword);
        qDebug() << objectName() + "::userLogin expected (password): " + retrievedPassword;
        return false;
    }
    //qDebug() << objectName() + "::userLogin matched (password): " + QString::fromLocal8Bit(hashedPw);
    //qDebug() << objectName() + "::userLogin to (password): " + targetIndex.data().toString();

    return true;
}

}
}
