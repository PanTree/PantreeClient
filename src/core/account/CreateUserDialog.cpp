﻿//
// Created by Stephen Hayes on 10/20/18.
//

#include "src/core/account/CreateUserDialog.h"
#include "src/core/account/UserCreator.h"
#include <QtWidgets>
namespace core
{
namespace account
{

CreateUserDialog::CreateUserDialog(QWidget *parent)
    :
      QWidget(parent),
      m_userCreator(new UserCreator(this))

{
    setObjectName("CreateUserDialog");
    connect(m_userCreator, &UserCreator::createUserSignal, this, &CreateUserDialog::createUserSignal);
    onInit();
}

void CreateUserDialog::onInit()
{
    m_mainLayout = new QVBoxLayout;
    setLayout(m_mainLayout);
    auto layout = new QFormLayout(this);
    auto userNameLineEdit = new QLineEdit(this);
    auto emailLineEdit = new QLineEdit(this);
    auto passwordLineEdit = new QLineEdit(this);

    passwordLineEdit->setEchoMode(QLineEdit::Password);
    layout->addRow("User Name: ", userNameLineEdit);
    layout->addRow("Email: ", emailLineEdit);
    layout->addRow("Password: ", passwordLineEdit);
    auto createUserButton = new QPushButton(QIcon(":/icons/Add.png"), "Create User", this);
    connect(createUserButton, &QPushButton::pressed, [=]()
    {
        const QString &username = userNameLineEdit->text();
        const QString &password = passwordLineEdit->text();
        const QString &email = emailLineEdit->text();
        if(m_userCreator->createUser(username, password, email))
        {
            emit logSignal(objectName(), "User Created!");
        }
        else
        {
        emit logSignal(objectName(), "User Creation failure!");
        }
    });
    m_mainLayout->addLayout(layout);
    m_mainLayout->addWidget(createUserButton);
}

}
}
