﻿//
// Created by Stephen Hayes on 10/12/18.
//

#pragma once
#include <QObject>
#include "src/core/enums/Enums.h"

namespace core
{
namespace account
{

class UserCreator : public QObject
{
Q_OBJECT
Q_DISABLE_COPY(UserCreator)

public:

    UserCreator(QObject *parent = Q_NULLPTR);
    bool createUser(const QString &username, const QString &password, const QString &email);

public slots:

signals:

    void logSignal(const QString &objectName, const QString &message, const core::LogPriority &priority = LogPriority::Normal);
    void createUserSignal(const QVariantList &data);

protected:
protected slots:
private:

    bool createInventoryData();
    bool createRecipeData();
    bool createListData();

    const QString m_dataDirectory{"user"};
    QString m_userDirectory;

private slots:
};

}
}

