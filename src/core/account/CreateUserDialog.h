﻿//
// Created by Stephen Hayes on 10/20/18.
//

#pragma once
#include <QWidget>
#include "src/core/enums/Enums.h"

class QVBoxLayout;

namespace core
{
namespace account
{
class UserCreator;
class CreateUserDialog : public QWidget
{
Q_OBJECT
Q_DISABLE_COPY(CreateUserDialog)

public:

    CreateUserDialog(QWidget *parent = nullptr);

public slots:

    void onInit();

signals:

    void logSignal(const QString &objectName, const QString &message, const core::LogPriority &priority = LogPriority::Normal);
    void createUserSignal(const QVariantList &data);

protected:
protected slots:
private:

    QVBoxLayout *m_mainLayout;
    UserCreator *m_userCreator;

private slots:

};

}
}

