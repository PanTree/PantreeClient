﻿//
// Created by Stephen Hayes on 10/12/18.
// Note: I'm well aware that under (almost) no circumstances should we implement
// our own crypto... that being said, this is a group project and I want to limit
// dependencies and extra frameworks.  Were this "for real", I would use bcrypt.
//

#include "src/core/account/UserCreator.h"
#include "src/core/parser/FileParser.h"

#include <QVariantList>
#include <QCryptographicHash>
#include <QDebug>
#include <QDir>

namespace core
{
namespace account
{

UserCreator::UserCreator(QObject *parent)
        : QObject(parent)
{
    setObjectName("UserCreator");
}

bool UserCreator::createUser(const QString &username, const QString &password, const QString &email)
{
    qDebug() << "createUser begin.";
    m_userDirectory = m_dataDirectory + "/" + username;
    QDir userDirectory(m_userDirectory);
    if(!userDirectory.exists())
    {
        emit logSignal(objectName(), "Creating user directory.");
        userDirectory.mkpath(".");
    }
    else
    {
        emit logSignal(objectName(), "User Already Exists!");
        return false;
    }
    if(!createInventoryData())
        return false;
    if(!createRecipeData())
        return false;
    if(!createListData())
        return false;

    QByteArray hashedPassword = QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Md5);
    QByteArray hexPassword = hashedPassword.toHex();

    QVariantList userData{
        {username},
        {hexPassword},
        {email},
        {m_userDirectory}
    };
    emit createUserSignal(userData);
    return true;
}

bool UserCreator::createInventoryData()
{
    QString inventory{m_userDirectory + "/inventory.dat"};
    qDebug() << "creating " + inventory;
    QVector<QVariantList> vec;
    utility::FileParser::writeTable(inventory, vec);
    return true;
}
bool UserCreator::createRecipeData()
{
    QString recipe{m_userDirectory + "/recipe.dat"};
    qDebug() << "creating " + recipe;
    QVector<QVariantList> vec;
    utility::FileParser::writeTable(recipe, vec);
    return true;
}
bool UserCreator::createListData()
{
    QString list{m_userDirectory + "/list.dat"};
    qDebug() << "creating " + list;
    QVector<QVariantList> vec;
    utility::FileParser::writeTable(list, vec);
    return true;
}

}
}
