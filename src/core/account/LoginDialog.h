﻿//
// Created by Stephen Hayes on 10/20/18.
//

#pragma once
#include <QWidget>
#include "src/core/enums/Enums.h"

class QLabel;
class QVBoxLayout;

namespace core
{
namespace account
{
class LoginDialog : public QWidget
{
Q_OBJECT
Q_DISABLE_COPY(LoginDialog)

public:

    LoginDialog(QWidget *parent = nullptr);

public slots:

    void onInit();
    void loginError(const QString &error);

signals:

    void logSignal(const QString &objectName, const QString &message, const core::LogPriority &priority = LogPriority::Normal);
    void loginSignal(const QString &username, const QString &password);

protected:
protected slots:
private:

    QLabel *m_errorLabel;
    QVBoxLayout *m_mainLayout;

private slots:

};

}
}

