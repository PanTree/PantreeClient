﻿//
// Created by Stephen Hayes on 10/20/18.
//

#include "src/core/account/LoginDialog.h"
#include <QtWidgets>

namespace core
{
namespace account
{

LoginDialog::LoginDialog(QWidget *parent)
    :
      QWidget(parent)

{
    setObjectName("LoginDialog");
    onInit();
}

void LoginDialog::onInit()
{
    m_mainLayout = new QVBoxLayout;
    setLayout(m_mainLayout);
    auto layout = new QFormLayout(this);
    auto userNameLineEdit = new QLineEdit(this);
    auto passwordLineEdit = new QLineEdit(this);

    passwordLineEdit->setEchoMode(QLineEdit::Password);
    layout->addRow("User Name: ", userNameLineEdit);
    layout->addRow("Password: ", passwordLineEdit);
    auto loginButton = new QPushButton(QIcon(":/icons/Start.png"), "Login", this);
    connect(loginButton, &QPushButton::pressed, [=]()
    {
        const QString &username = userNameLineEdit->text();
        const QString &password = passwordLineEdit->text();
        emit loginSignal(username, password);
    });
    m_errorLabel = new QLabel("Please enter your information: ");
    m_mainLayout->addWidget(m_errorLabel);
    m_mainLayout->addLayout(layout);
    m_mainLayout->addWidget(loginButton);
}

void LoginDialog::loginError(const QString &msg)
{
    m_errorLabel->setText(msg);
}

}
}
