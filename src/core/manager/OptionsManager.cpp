﻿//
//Created by Cedrik Acosta.
//

#include "src/core/manager/OptionsManager.h"
#include <QtWidgets>


namespace core
{
namespace manager
{

OptionManager::OptionManager(const QString &username, QWidget *parent)
    : BaseManager(parent),
      m_userName(username),
      m_mainLayout(new QVBoxLayout(this))
{
    setUI();
    setData();
}

void OptionManager::setUI()
{
    //TODO: Make the actual window to the point where it works.
    // - GroupBox with "Units" header, Metric/Imperial Units as exclusive radio buttons.
    // - Abstract it enough to properly be able to add/remove options.
    // - Two PushButtons: Save and Cancel

    qDebug() << "setUI() begin.";
    auto measurementLayout = new QFormLayout;
    auto testLabel = new QLabel(this); /// SH, just for demonstrating how to use these...
    measurementLayout->addWidget(new QLabel("Measurement Configuration", this));
    measurementLayout->addWidget(testLabel);
    auto metricButton = new QRadioButton(this);
    auto imperialButton = new QRadioButton(this);
    measurementLayout->addRow("Metric: ", metricButton);
    measurementLayout->addRow("Imperial: ", imperialButton);
    m_buttonGroup = new QButtonGroup;
    m_buttonGroup->addButton(metricButton, 0);
    m_buttonGroup->addButton(imperialButton, 1);

    /// this signal can propogate anywhere we want... not just manipulate a local label.

    connect(m_buttonGroup, QOverload<int, bool>::of(&QButtonGroup::buttonToggled),
        [=](int id, bool checked)
    {
        testLabel->setText("Button ID: " + QString::number(id) + " toggled.");
    });

    m_mainLayout->addLayout(measurementLayout);

    auto syncButton = new QPushButton(this);
    syncButton->setText("Sync To Server");
    m_mainLayout->addWidget(syncButton);
}

/// Shouldn't be necessary if the Unit Radio Buttons can propogate throughout(?).
/*void OptionManager::setConfig()
{
    //TODO: Save custom config
    //      - How is the config set?
    //      - Is it a variable? Bool?
    //      - Attach it to the json data, perhaps?

    // Config thoughts: For metric/imperial, have a bool.
    //                  If 0, make it metric.
    //                  If 1, make it imperial.
    //                  Pass units through Conversion Helper before sending data to .cpps?
}*/

bool OptionManager::testOnline()
{
    //TODO:: Check if the Client is online, and then sync appropriate options.
    return false;
}

}
}
