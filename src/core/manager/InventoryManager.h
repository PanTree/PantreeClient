﻿//
// Created by Stephen Hayes on 9/29/18.
//

#pragma once

#include "src/core/manager/BaseManager.h"

class QComboBox;
class QFormLayout;
class QHBoxLayout;
class QItemSelectionModel;
class QLineEdit;
class QPushButton;
class QTableView;
class QToolButton;
class QVBoxLayout;

namespace core
{
namespace model
{
class InventoryModel;
}
namespace utility
{
class ConversionHelper;
}
namespace manager
{

class InventoryManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(InventoryManager)

public:

    InventoryManager(const QString &username, QWidget *parent);

public slots:

    void checkInventorySlot(QVariantList &data);

signals:
protected:

    void setData() override {}
    void setUI() override;

protected slots:
private:

    void setModel();
    void addItem();
    void removeItem();
    void clearData();

    core::model::InventoryModel *m_inventoryModel;
    QString m_userName;

    QComboBox *m_typeCombo;
    QFormLayout *m_buttonLayout;
    QHBoxLayout *m_topLayout;
    QHBoxLayout *m_centralLayout;
    QLineEdit *m_entryName;
    QLineEdit *m_entryQuantity;
    QTableView *m_modelView;
    QToolButton *m_addItemButton;
    QToolButton *m_removeItemButton;
    QToolButton *m_clearDataButton;
    QVBoxLayout *m_mainLayout;

private slots:

};

}
}

