﻿//
// Created by Ryan Dill on 10/28/18.
//

#pragma once

#include "src/core/manager/BaseManager.h"

class QDataWidgetMapper;
class QFormLayout;
class QHBoxLayout;
class QLabel;
class QItemSelection;
class QPlainTextEdit;
class QPushButton;
class QTableView;
class QToolButton;
class QVBoxLayout;

namespace core
{
namespace model
{
class RecipeModel;
}
namespace manager
{

class RecipeManager: public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(RecipeManager)

    public:

        RecipeManager(const QString &username, QWidget *parent);

    protected:

        void setData() override {}
        void setUI() override;

    private:

        void clearData();
        void createRecipe();
        void editRecipe();
        void removeItem();
        void setModel();

        core::model::RecipeModel *m_recipeModel;
        QString m_userName;

        QDataWidgetMapper *m_dataMapper;
        QFormLayout *m_buttonLayout;
        QHBoxLayout *m_centralLayout;
        QLabel *m_recipeLabel;
        QPlainTextEdit *m_recipeDisplay;
        QTableView *m_modelView;
        QToolButton *m_createRecipeButton;
        QToolButton *m_removeRecipeButton;
        QToolButton *m_editRecipeButton;
        QToolButton *m_clearDataButton;
        QVBoxLayout *m_mainLayout;
        QVBoxLayout *m_bottomLayout;

    public slots:

        void getRecipesSlot(QVariantList &data);
        void getRecipeByNameSlot(const QString &name, QVariantList &data);

    private slots:

        void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

};

}
}
