﻿//
// Created by Stephen Hayes on 9/28/18.
//

#include <QDebug>
#include <QFile>

#include "src/core/manager/BaseManager.h"
#include "src/core/parser/FileParser.h"

namespace core
{
namespace manager
{

BaseManager::BaseManager(QWidget *parent)
:QWidget(parent)
{}

void BaseManager::init()
{
    QString name(metaObject()->className());
    QString trueName(name.section("::", -1));
    setObjectName(trueName);
    parseConfig();
    setData();
    emit logSignal(objectName(), "loaded", LogPriority::PositiveAlert);
}

void BaseManager::parseConfig()
{
    qDebug() << "beginning.";
    QString configString(":" + objectName() + ".json");
    qDebug() << "(configString): " + configString;

    m_configMap = utility::FileParser::parseJson(configString);
    if(m_configMap.contains("error"))
    {
        emit logSignal(objectName(), "config parse error.", LogPriority::Warning);
        qCritical() << "config parse error.";
    }
    else
    {
        emit logSignal(objectName(), "config parsed.", LogPriority::Normal);
        qDebug() << "config parsed.";
    }
}

void BaseManager::checkData()
{
    QString datapath = m_configMap.find("datapath").value().toString();
    QFile file(datapath);
    if(file.exists())
    {
        setData();
    }
    else
    {
        qWarning() << "no stored data loaded.";
    }
}

}
}
