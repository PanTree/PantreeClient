﻿//
// Created by Stephen Hayes on 10/12/18.
//

#include "src/core/manager/AccountManager.h"
#include "src/core/model/UserModel.h"
#include "src/core/account/CreateUserDialog.h"
#include "src/core/account/LoginDialog.h"

#include <QtWidgets>

namespace core
{
namespace manager
{

AccountManager::AccountManager(QWidget *parent)
        : BaseManager(parent),
          m_createUserDialog(new account::CreateUserDialog(this)),
          m_loginDialog(new account::LoginDialog(this)),
          m_userModel(new model::UserModel(this)),
          m_modelView(new QTableView(this)),
          m_mainLayout(new QVBoxLayout(this))
{
    setUI();
    setModel();
    setConnections();
}

void AccountManager::setUI()
{
    m_createUserButton = new QPushButton(QIcon(":/icons/Add.png"), "Create User", this);
    m_loginButton = new QPushButton(QIcon(":/icons/Affirmative.png"), "Login", this);
    m_logoutButton = new QPushButton(QIcon(":/icons/Suspend.png"), "Logout", this);
    m_logoutButton->setEnabled(false);
    auto buttonLayout = new QVBoxLayout;
    buttonLayout->addWidget(m_createUserButton);
    buttonLayout->addWidget(m_loginButton);
    buttonLayout->addWidget(m_logoutButton);
    m_mainLayout->addLayout(buttonLayout);
    m_createUserDialog->setWindowFlags(Qt::Dialog);
    m_createUserDialog->setVisible(false);
    m_loginDialog->setWindowFlags(Qt::Dialog);
    m_loginDialog->setVisible(false);
}

void AccountManager::setModel()
{
    connect(m_userModel, &model::BaseModel::logSignal, this, &BaseManager::logSignal);
    m_userModel->init();
    m_modelView->setModel(m_userModel);
    m_modelView->setSelectionMode(QAbstractItemView::SingleSelection);
    m_modelView->hideColumn(1);
    m_modelView->hideColumn(2);
    m_modelView->hideColumn(3);
    auto userLabel = new QLabel("Registered Users", this);
    m_mainLayout->addWidget(userLabel);
    m_mainLayout->addWidget(m_modelView);
}

void AccountManager::setConnections()
{
    connect(m_createUserButton, &QAbstractButton::clicked, this, &AccountManager::createUser);
    connect(m_loginButton, &QAbstractButton::clicked, this, &AccountManager::login);
    connect(m_logoutButton, &QAbstractButton::clicked, this, &AccountManager::logoutSignal);
    connect(m_createUserDialog, &account::CreateUserDialog::createUserSignal, this, &AccountManager::tryCreateUserSlot);
    connect(m_loginDialog, &account::LoginDialog::loginSignal, this, &AccountManager::tryLoginSlot);
}

void AccountManager::createUser()
{
    m_createUserDialog->show();
}

void AccountManager::tryCreateUserSlot(const QVariantList &data)
{
    qDebug() << "begin.";
    m_userModel->processAddItem(data);
    m_createUserDialog->hide();
}

void AccountManager::login()
{
    m_loginDialog->show();
}

void AccountManager::tryLoginSlot(const QString &username, const QString &password)
{
    if(m_userModel->userLogin(username, password))
    {
        loggedInSlot();
        emit loginSignal(username);
        m_loginDialog->hide();
    }
    else
    {
        m_loginDialog->loginError("Incorrect username or password.");
    }
}

void AccountManager::loggedInSlot()
{
    m_loginButton->setEnabled(false);
    m_createUserButton->setEnabled(false);
    m_logoutButton->setEnabled(true);
}

void AccountManager::loggedOutSlot()
{
    m_loginButton->setEnabled(true);
    m_createUserButton->setEnabled(true);
    m_logoutButton->setEnabled(false);
}

}
}
