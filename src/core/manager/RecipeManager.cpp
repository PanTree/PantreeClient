﻿//
// Created by Ryan Dill on 10/28/18.
//

#include "src/core/manager/RecipeManager.h"
#include "src/core/model/RecipeModel.h"
#include "src/core/parser/ConversionHelper.h"
#include <QtWidgets>
#include <QItemSelectionModel>


namespace core
{
namespace manager
{

RecipeManager::RecipeManager(const QString &username, QWidget *parent)
    : BaseManager(parent),
      m_recipeModel(new core::model::RecipeModel(username, this)),
      m_userName(username),
      m_modelView(new QTableView(this)),
      m_mainLayout(new QVBoxLayout(this))

{
    setUI();
    setModel();

    /// lets try this out
    m_dataMapper = new QDataWidgetMapper(this);
    m_dataMapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);
    m_dataMapper->setModel(m_recipeModel);
    connect(m_modelView->selectionModel(), &QItemSelectionModel::currentRowChanged, m_dataMapper, &QDataWidgetMapper::setCurrentModelIndex);
}

void RecipeManager::setUI()
{
    qDebug() << objectName() + "::setUI() begin.";
    m_createRecipeButton=new QToolButton(this);
    m_createRecipeButton->setIcon(QIcon(":/icons/Add.png"));
    m_removeRecipeButton=new QToolButton(this);
    m_removeRecipeButton->setIcon(QIcon(":/icons/Delete.png"));
    m_editRecipeButton=new QToolButton(this);
    m_editRecipeButton->setIcon(QIcon(":/icons/Recipe.png"));
    m_editRecipeButton->setDisabled(true);
    m_clearDataButton=new QToolButton(this);
    m_clearDataButton->setIcon(QIcon(":/icons/Abort.png"));

    m_centralLayout = new QHBoxLayout;
    m_buttonLayout = new QFormLayout;
    m_centralLayout->addLayout(m_buttonLayout);
    m_centralLayout->addWidget(m_modelView);
    m_mainLayout->addLayout(m_centralLayout);

    m_buttonLayout->addRow("Create Recipe:", m_createRecipeButton);
    m_buttonLayout->addRow("Remove Recipe:", m_removeRecipeButton);
    m_buttonLayout->addRow("Edit Recipe", m_editRecipeButton);
    m_buttonLayout->addRow("Clear Recipes:", m_clearDataButton);

    m_recipeLabel = new QLabel(this);
    m_recipeDisplay = new QPlainTextEdit(this);
    m_bottomLayout = new QVBoxLayout;
    m_bottomLayout->addWidget(m_recipeLabel);
    m_bottomLayout->addWidget(m_recipeDisplay);
    m_mainLayout->addLayout(m_bottomLayout);

    connect(m_createRecipeButton, &QAbstractButton::pressed, this, &RecipeManager::createRecipe);
    connect(m_removeRecipeButton, &QAbstractButton::pressed, this, &RecipeManager::removeItem);
    connect(m_clearDataButton, &QAbstractButton::pressed, this, &RecipeManager::clearData);
    connect(m_editRecipeButton, &QAbstractButton::pressed, this, &RecipeManager::editRecipe);
}

void RecipeManager::setModel()
{
    connect(m_recipeModel, &model::BaseModel::logSignal, this, &BaseManager::logSignal);
    m_recipeModel->init();
    m_modelView->setModel(m_recipeModel);
    m_modelView->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(m_modelView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &RecipeManager::selectionChanged);
}

/// <SH> this whole function is a memory leak... everything gets created on recipe creation but no cleanup until shut down.
/// just annotating in case someone says "thats a memory leak".  Yea.  I know.  It should be its own class and constructed
/// on manager instantiation.
void RecipeManager::createRecipe()
{
    auto mainLayout = new QVBoxLayout(this);
    auto unitTypeCombo = new QComboBox(this);
    unitTypeCombo->setEnabled(false);
    auto recipeNameEdit = new QLineEdit(this);
    auto entryNameEdit = new QLineEdit(this);
    auto entryQuantityEdit = new QLineEdit(this);

    auto volumeButton = new QRadioButton("Volume");
    auto weightButton = new QRadioButton("Weight");
    auto buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(volumeButton, 0);
    buttonGroup->addButton(weightButton, 1);

    connect(buttonGroup, QOverload<int, bool>::of(&QButtonGroup::buttonToggled),
            [&](int id, bool checked)
    {
        unitTypeCombo->clear();
        (id == 0) ? unitTypeCombo->addItems(utility::getVolumeTypes()) : unitTypeCombo->addItems(utility::getWeightTypes());
        unitTypeCombo->setEnabled(true);
    });

    auto typeLayout = new QHBoxLayout(this);
    typeLayout->addWidget(volumeButton);
    typeLayout->addWidget(weightButton);

    mainLayout->addLayout(typeLayout);
    auto getItemLayout = new QFormLayout(this);
    getItemLayout->addRow("Recipe Name", recipeNameEdit);
    getItemLayout->addRow("Item Name", entryNameEdit);
    getItemLayout->addRow("Item Quantity", entryQuantityEdit);
    getItemLayout->addRow("Unit Type", unitTypeCombo);
    mainLayout->addLayout(getItemLayout);

    auto buttonLayout = new QHBoxLayout(this);
    auto confirmButton = new QPushButton("Confirm", this);
    confirmButton->setEnabled(false);
    auto addButton = new QPushButton("Add", this);
    connect(recipeNameEdit, &QLineEdit::textChanged, [&](){addButton->setEnabled(true);});
    addButton->setEnabled(false);
    auto cancelButton = new QPushButton("Cancel", this);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(confirmButton);

    auto tableModel = new QTableWidget(this);
    tableModel->setColumnCount(3);
    tableModel->setHorizontalHeaderLabels(QStringList{"Name", "Quantity", "Unit Type"});

    mainLayout->addWidget(tableModel);
    mainLayout->addLayout(buttonLayout);
    QDialog dialog;
    dialog.setLayout(mainLayout);
    QVariantList result;
    connect(addButton, &QPushButton::pressed, [&]()
    {
        if(result.isEmpty())
            result.append(recipeNameEdit->text());
        result.append(entryNameEdit->text());
        result.append(entryQuantityEdit->text().toDouble());
        result.append(unitTypeCombo->currentText());

        auto newItemName = new QTableWidgetItem(entryNameEdit->text());
        auto newItemQuantity = new QTableWidgetItem(entryQuantityEdit->text());
        auto newItemUnits = new QTableWidgetItem(unitTypeCombo->currentText());
        int row = tableModel->rowCount();
        tableModel->insertRow(row);
        tableModel->setItem(row, 0, newItemName);
        tableModel->setItem(row, 1, newItemQuantity);
        tableModel->setItem(row, 2, newItemUnits);
        entryNameEdit->clear();
        entryQuantityEdit->clear();
        confirmButton->setEnabled(true);
    }
    );

    connect(confirmButton, &QPushButton::pressed, &dialog, &QDialog::accept);
    connect(cancelButton, &QPushButton::pressed, &dialog, &QDialog::reject);

    dialog.exec();
    if(dialog.result() == 1)
    {
        qDebug() << result;
        m_recipeModel->processAddItem(result);
    }
}

/// this is probably broken since I was testing stack instantiation as opposed to heap
/// Qt really doesn't like stack instantiation and I only barely understand why.

void RecipeManager::editRecipe()
{
    QDialog dialog;
    QVBoxLayout layout;
    QFormLayout formLayout;
    dialog.setLayout(&layout);
    QLineEdit recipeName;
    QLineEdit itemName;
    QLineEdit itemQuantity;
    QComboBox itemType;

    formLayout.addRow("Item Name", &itemName);
    formLayout.addRow("Item Quantity", &itemQuantity);
    formLayout.addRow("Unit Type", &itemType);

    m_dataMapper->addMapping(&itemName, 0);
    m_dataMapper->addMapping(&itemQuantity, 1);
    m_dataMapper->addMapping(&itemType, 2);

    QPushButton nextButton("Next", this);
    QPushButton previousButton("Previous", this);
    QPushButton submitButton("Submit", this);
    QPushButton cancelButton("Cancel", this);
    QGridLayout buttonLayout;

    buttonLayout.addWidget(&previousButton, 0, 0, 1, 1);
    buttonLayout.addWidget(&nextButton, 0, 1, 1, 1);
    buttonLayout.addWidget(&cancelButton, 1, 0, 1, 1);
    buttonLayout.addWidget(&submitButton, 1, 1, 1, 1);

    connect(&previousButton, &QAbstractButton::pressed, [&]()
    {
        m_dataMapper->toLast();
    });

    connect(&nextButton, &QAbstractButton::pressed, [&]()
    {
        m_dataMapper->toNext();
    });



    connect(&submitButton, &QPushButton::pressed, [&]()
    {
        m_dataMapper->submit();
        dialog.accept();
    });
    connect(&cancelButton, &QPushButton::pressed, &dialog, &QDialog::reject);

    layout.addLayout(&formLayout);
    layout.addLayout(&buttonLayout);


    m_dataMapper->toNext();
    dialog.exec();
}

/*
void RecipeManager::editRecipe()
{
    auto selectedIndex = m_modelView->selectionModel()->currentIndex();
    qDebug() << objectName() + "::editRecipe attempting to edit (ROW: " + QString::number(selectedIndex.row());

    auto recipeNameActual = m_recipeModel->data(selectedIndex, Qt::DisplayRole).toString();
    auto recipeActual = m_recipeModel->data(selectedIndex, Qt::UserRole).toList();

    auto unitTypeCombo = new QComboBox(this);
    auto recipeName = new QLineEdit(this);
    recipeName->setText(recipeNameActual);
    auto entryName = new QLineEdit(this);
    auto entryQuantity = new QLineEdit(this);
    QStringList types{"Grams", "Kilograms", "Ounces", "Pounds", "Mililiter", "Liter", "Pint", "Cup", "Quart", "Gallon", "Fluid Ounce", "Tablespoon", "Teaspoon"};
    unitTypeCombo->addItems(types);
    auto getItemLayout = new QFormLayout;
    getItemLayout->addRow("Recipe Name", recipeName);
    getItemLayout->addRow("Item Name", entryName);
    getItemLayout->addRow("Item Quantity", entryQuantity);
    getItemLayout->addRow("Unit Type", unitTypeCombo);
    auto buttonLayout = new QHBoxLayout;
    auto confirmButton = new QPushButton("Confirm", this);
    confirmButton->setEnabled(true);
    auto addButton = new QPushButton("Add", this);
    connect(entryQuantity, &QLineEdit::textChanged, [&](){addButton->setEnabled(true);});
    addButton->setEnabled(false);
    auto cancelButton = new QPushButton("Cancel", this);
    auto removeButton = new QPushButton("Remove", this);
    removeButton->setEnabled(true);

    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(confirmButton);
    buttonLayout->addWidget(removeButton);
    auto mainLayout = new QVBoxLayout;
    mainLayout->addLayout(getItemLayout);

    auto tableModel = new QTableWidget(this);
    tableModel->setColumnCount(3);
    auto header0 = new QTableWidgetItem("Name");
    auto header1 = new QTableWidgetItem("Quantity");
    auto header2 = new QTableWidgetItem("Units");
    tableModel->setHorizontalHeaderItem(0, header0);
    tableModel->setHorizontalHeaderItem(1, header1);
    tableModel->setHorizontalHeaderItem(2, header2);

    // Add Combo Boxes to the units column
    for(int i = 0; i < tableModel->rowCount(); i++)
    {
        QComboBox *unitComboBox = new QComboBox(this);
        unitComboBox->addItems(types);
        tableModel->setCellWidget(i, 2, unitComboBox);
    }

    // Populates the tableModel with the recipe values
    int row;
    int col;
    // Move the QVariantList up here to add the recipe values
    QVariantList result;
    result.append(recipeName->text());
    tableModel->setRowCount(recipeActual.length()/tableModel->columnCount());
    int itemNum = 0;
    // Pop the first one off since its the recipe name
    recipeActual.pop_front();
    result.append(recipeActual);
    for(row = 0; row < tableModel->rowCount(); row++)
        for(col = 0; col < tableModel->columnCount(); col++)
        {
            if(col == 2)
            {
                QComboBox *unitComboBox = new QComboBox(this);
                unitComboBox->addItems(types);
                tableModel->setCellWidget(row, col, unitComboBox);
                int index = unitComboBox->findText(recipeActual.value(itemNum).toString());
                if (index != -1)
                    unitComboBox->setCurrentIndex(index);
                itemNum++;
            }
            else
            {
                auto newItem = new QTableWidgetItem(recipeActual.value(itemNum).toString());
                tableModel->setItem(row, col, newItem);
                itemNum++;
            }
        }

    mainLayout->addWidget(tableModel);
    mainLayout->addLayout(buttonLayout);
    QDialog dialog;
    dialog.setLayout(mainLayout);
    connect(addButton, &QPushButton::pressed, [&]()
    {
        result.append(entryName->text());
        result.append(entryQuantity->text().toDouble());
        result.append(unitTypeCombo->currentText());
        qDebug() << result.length();
        auto newItemName = new QTableWidgetItem(entryName->text());
        auto newItemQuantity = new QTableWidgetItem(entryQuantity->text());
        auto newItemUnits = new QTableWidgetItem(unitTypeCombo->currentText());
        int row = tableModel->rowCount();
        tableModel->insertRow(row);
        tableModel->setItem(row, 0, newItemName);
        tableModel->setItem(row, 1, newItemQuantity);
        tableModel->setItem(row, 2, newItemUnits);
        QComboBox *jBox = new QComboBox(this);
        jBox->addItems(types);
        tableModel->setCellWidget(row, 2, jBox);
        int index = jBox->findText(unitTypeCombo->currentText());
        if (index != -1)
            jBox->setCurrentIndex(index);
        entryName->clear();
        entryQuantity->clear();
        confirmButton->setEnabled(true);
    }
    );
    connect(removeButton, &QPushButton::pressed, [&]()
    {
        auto index = tableModel->selectionModel()->currentIndex();

        tableModel->removeRow(index.row());

        // I haven't had any luck getting this to work correctly.  But it LOOKS like it works.
        // Leaving this feature in for demonstration purposes.
    }
    );

    connect(confirmButton, &QPushButton::pressed, &dialog, &QDialog::accept);
    connect(cancelButton, &QPushButton::pressed, &dialog, &QDialog::reject);


    dialog.exec();
    if(dialog.result() == 1)
    {
        qDebug() << result;
        RecipeManager::removeItem();
        m_recipeModel->processAddItem(result);
        m_editRecipeButton->setDisabled(true);
    }
}
*/
void RecipeManager::removeItem()
{
    auto selectedIndex = m_modelView->selectionModel()->currentIndex();
    qDebug() << objectName() + "::removeItem attempting to remove (ROW): " + QString::number(selectedIndex.row());
    m_recipeModel->processRemoveItem(selectedIndex);
}

void RecipeManager::clearData()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::critical(this, tr("QMessageBox::critical()"),
                                  QString("Are you sure you want to clear all the data?"),
                                  QMessageBox::Cancel | QMessageBox::Ok);
    if (reply == QMessageBox::Cancel)
        return;

    m_recipeModel->processClearData();
}

void RecipeManager::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    /// Q_UNUSED is a macro that just tells the compiler we know we aren't using this so stop complaining...
    Q_UNUSED(deselected);

    // If the selection is changed, enable the edit recipe button
    m_editRecipeButton->setDisabled(false);

    /// Clear out the old list.
    m_recipeDisplay->clear();

    /// the selection model passed in all the selected indexes, we just need one since we're asking for the entire row anyway.
    /// if empty, just return.
    auto indexList = selected.indexes();
    if(indexList.isEmpty())
        return;
    auto index = indexList.at(0); /// just took the first index

    /// now we ask the model for the entire row, it gives us a QVariant.
    auto data = m_recipeModel->data(index, Qt::UserRole);
    qDebug() << data;

    /// Convert the QVariant to a List.
    /// we know its a List since we wrote the data function.
    auto list = data.toList();
    qDebug() << list;

    /// now lets iteratte through it, but we first pop off the first entry since we know its our list name
    /// and that belongs in our label ABOVE the list display
    QListIterator<QVariant> listIt(list);
    m_recipeLabel->setText(listIt.next().toString());

    /// loop time: we know the format is "label" followed by "quantity"
    /// each call to .next() advances the iterator
    /// when we have our set, we shove it into the display.

    while(listIt.hasNext())
    {
        auto label = listIt.next().toString();
        auto quantity = listIt.next().toString();
        auto units = listIt.next().toString();
        m_recipeDisplay->appendPlainText(label + " " + quantity + " " + units);
    }
}


void RecipeManager::getRecipesSlot(QVariantList &data)
{
    qDebug() << "Recipes Slot Triggered";

    int row = 0;
    int column = 0;

    for (row = 0; row < m_recipeModel->rowCount(); row++)
    {
        auto index = m_recipeModel->index(row, column);
        auto recipeName = m_recipeModel->data(index, Qt::DisplayRole).toString();
        data.append(recipeName);
    }

}

void RecipeManager::getRecipeByNameSlot(const QString &name, QVariantList &data)
{
    qDebug() << "RecipeByNameSlot Triggered";

    int row = 0;
    int column = 0;

    for (row = 0; row < m_recipeModel->rowCount(); row++)
    {
        auto index = m_recipeModel->index(row, column);

        auto recipeName = m_recipeModel->data(index, Qt::DisplayRole).toString();

        if (recipeName == name)
        {
            auto recipe = m_recipeModel->data(index, Qt::UserRole).toList();
            data.append(recipe);
        }
    }
}

}
}
