﻿//
// Created by tdangol on 10/24/18.
//

#include "src/core/parser/ConversionHelper.h"
#include "src/core/manager/ListManager.h"
#include "src/core/model/ListModel.h"

#include <QtWidgets>

namespace core
{
namespace manager
{

ListManager::ListManager(const QString &username, QWidget *parent)
    : BaseManager(parent),
      m_listModel(new core::model::ListModel(username, this)),
      m_userName(username),
      m_modelView(new QTableView(this)),
      m_mainLayout(new QVBoxLayout(this))
{
    setUI();
    setModel();
}

void ListManager::setUI()
{
    qDebug() << objectName() + "::setUI() begin.";
    m_addItemButton=new QToolButton(this);
    m_addItemButton->setIcon(QIcon(":/icons/Add.png"));
    m_removeItemButton=new QToolButton(this);
    m_removeItemButton->setIcon(QIcon(":/icons/Delete.png"));
    m_clearDataButton=new QToolButton(this);
    m_clearDataButton->setIcon(QIcon(":/icons/Abort.png"));

    m_generateListButton=new QToolButton(this);
    m_generateListButton->setIcon(QIcon(":/icons/ViewArchive.png"));

    m_editListButton=new QToolButton(this);
    m_editListButton->setIcon(QIcon(":/icons/Recipe.png"));

    m_listTitle=new QLabel("List Title");
    m_listDisplay=new QPlainTextEdit(this);
    m_bottomLayout=new QVBoxLayout(this);

    m_bottomLayout->addWidget(m_listTitle);
    m_bottomLayout->addWidget(m_listDisplay);

    m_topLayout = new QHBoxLayout;

    m_mainLayout->addLayout(m_topLayout);

    m_centralLayout = new QHBoxLayout;
    m_buttonLayout = new QFormLayout;
    m_centralLayout->addLayout(m_buttonLayout);
    m_centralLayout->addWidget(m_modelView);
    m_mainLayout->addLayout(m_centralLayout);
    m_mainLayout->addLayout(m_bottomLayout);

    m_buttonLayout->addRow("Generate Custom Shopping List", m_addItemButton);
    m_buttonLayout->addRow("Remove Shopping List", m_removeItemButton);
    m_buttonLayout->addRow("Clear All Lists", m_clearDataButton);
    m_buttonLayout->addRow("Generate List from Recipe", m_generateListButton);
    m_buttonLayout->addRow("Edit List ", m_editListButton);

    connect(m_addItemButton, &QAbstractButton::pressed, this, &ListManager::addItem);
    connect(m_removeItemButton, &QAbstractButton::pressed, this, &ListManager::removeItem);
    connect(m_clearDataButton, &QAbstractButton::pressed, this, &ListManager::clearData);
    connect(m_generateListButton, &QAbstractButton::pressed, this, &ListManager::generateList);
    connect(m_editListButton, &QAbstractButton::pressed, this, &ListManager::editList);
}

void ListManager::setModel()
{
    connect(m_listModel, &model::BaseModel::logSignal, this, &BaseManager::logSignal);
    m_listModel->init();
    m_modelView->setModel(m_listModel);
    m_modelView->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(m_modelView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ListManager::selectionChanged);
}

void ListManager::generateList()
{
    m_listDisplay->clear();
    QVariantList recipeList;
    QStringList recipeNames;
    emit getRecipeSignal(recipeList);
    qDebug() << recipeList;

    QListIterator <QVariant> recipeListIt(recipeList);
    while(recipeListIt.hasNext())
    {
        recipeNames.append(recipeListIt.next().toString());
    }

    bool ok;
    QString recipeChoice=QInputDialog::getItem(this, tr("QInputDialog::getItems()"),tr("Choose a recipe :"),
                                               recipeNames, 0, false, &ok);
    if(ok)
    {
        QVariantList recipe;
        emit getRecipeByNameSignal(recipeChoice, recipe);

        qDebug() << recipe;

        emit checkInventorySignal(recipe);

        qDebug() << recipe;

        QListIterator <QVariant> listIt(recipe);
        QVariantList baseList;
        baseList.append(listIt.next());
        while(listIt.hasNext())
        {
            auto label = listIt.next().toString();
            auto quantity = listIt.next().toDouble();
            auto unit = listIt.next().toString();

            if (quantity > 0)
            {
                baseList.append(label);
                baseList.append(quantity);
                baseList.append(unit);
            }
        }

        if (baseList.count()==1)
        {
            QMessageBox msgBox;
            msgBox.setText("All items are in the inventory");
            msgBox.exec();
        }
        if(baseList.count() > 1)
        {
            m_listModel->processAddItem(baseList);
        }
        qDebug() << baseList;
    }
    else
    {
        qCritical() << "Invalid entry";
        return;
    }
}

void ListManager::addItem()
{

    auto mainLayout = new QVBoxLayout(this);
    auto unitTypeCombo = new QComboBox(this);
    unitTypeCombo->setEnabled(false);
    auto listNameEdit = new QLineEdit(this);
    auto entryNameEdit = new QLineEdit(this);
    auto entryQuantityEdit = new QLineEdit(this);

    auto volumeButton = new QRadioButton("Volume");
    auto weightButton = new QRadioButton("Weight");
    auto buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(volumeButton, 0);
    buttonGroup->addButton(weightButton, 1);

    connect(buttonGroup, QOverload<int, bool>::of(&QButtonGroup::buttonToggled),
            [&](int id, bool checked)
            {
                unitTypeCombo->clear();
                (id == 0) ? unitTypeCombo->addItems(utility::getVolumeTypes()) : unitTypeCombo->addItems(utility::getWeightTypes());
                unitTypeCombo->setEnabled(true);
            });

    auto typeLayout = new QHBoxLayout(this);
    typeLayout->addWidget(volumeButton);
    typeLayout->addWidget(weightButton);

    mainLayout->addLayout(typeLayout);
    auto getItemLayout = new QFormLayout(this);
    getItemLayout->addRow("List Name", listNameEdit);
    getItemLayout->addRow("Item Name", entryNameEdit);
    getItemLayout->addRow("Item Quantity", entryQuantityEdit);
    getItemLayout->addRow("Unit Type", unitTypeCombo);
    mainLayout->addLayout(getItemLayout);

    auto buttonLayout = new QHBoxLayout(this);
    auto confirmButton = new QPushButton("Confirm", this);
    confirmButton->setEnabled(false);
    auto addButton = new QPushButton("Add", this);
    connect(listNameEdit, &QLineEdit::textChanged, [&](){addButton->setEnabled(true);});
    addButton->setEnabled(false);
    auto cancelButton = new QPushButton("Cancel", this);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(confirmButton);

    auto tableModel = new QTableWidget(this);
    tableModel->setColumnCount(3);
    tableModel->setHorizontalHeaderLabels(QStringList{"Name", "Quantity", "Unit Type"});

    mainLayout->addWidget(tableModel);
    mainLayout->addLayout(buttonLayout);
    QDialog dialog;
    dialog.setLayout(mainLayout);
    QVariantList result;
    connect(addButton, &QPushButton::pressed, [&]()
            {
                if(result.isEmpty())
                    result.append(listNameEdit->text());
                result.append(entryNameEdit->text());
                result.append(entryQuantityEdit->text().toDouble());
                result.append(unitTypeCombo->currentText());

                auto newItemName = new QTableWidgetItem(entryNameEdit->text());
                auto newItemQuantity = new QTableWidgetItem(entryQuantityEdit->text());
                auto newItemUnits = new QTableWidgetItem(unitTypeCombo->currentText());
                int row = tableModel->rowCount();
                tableModel->insertRow(row);
                tableModel->setItem(row, 0, newItemName);
                tableModel->setItem(row, 1, newItemQuantity);
                tableModel->setItem(row, 2, newItemUnits);
                entryNameEdit->clear();
                entryQuantityEdit->clear();
                confirmButton->setEnabled(true);
            }
    );

    connect(confirmButton, &QPushButton::pressed, &dialog, &QDialog::accept);
    connect(cancelButton, &QPushButton::pressed, &dialog, &QDialog::reject);

    dialog.exec();
    if(dialog.result() == 1)
    {
        qDebug() << result;
        m_listModel->processAddItem(result);
    }
}

void ListManager::removeItem()
{
    auto selectedIndex = m_modelView->selectionModel()->currentIndex();
    qDebug() << objectName() + "::removeItem attempting to remove (ROW): " + QString::number(selectedIndex.row());
    m_listModel->processRemoveItem(selectedIndex);
}

void ListManager::clearData()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::critical(this, tr("QMessageBox::critical()"),
                                  QString("Are you sure you want to clear all the data?"),
                                  QMessageBox::Cancel | QMessageBox::Ok);
    if (reply == QMessageBox::Cancel)
        return;

    m_listModel->processClearData();
}

void ListManager::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(deselected);
    m_listDisplay->clear();
    auto indexList = selected.indexes();

    if(indexList.isEmpty())
    {
        return;
    }

    auto index = indexList.at(0);
    auto list = m_listModel->data(index, Qt::UserRole).toList();

    QListIterator<QVariant> listIt(list);
    m_listTitle->setText(listIt.next().toString());

    /// loop time: we know the format is "label" followed by "quantity" followed by "unit type"
    /// each call to .next() advances the iterator
    /// when we have our set, we shove it into the display.

    qDebug() << list;
    while(listIt.hasNext())
    {
        auto label = listIt.next().toString();
        auto quantity = listIt.next().toString();
        auto unit = listIt.next().toString();
        m_listDisplay->appendPlainText(label + " " + quantity + " " + unit);
    }
}

void ListManager::editList()
{
    auto selectedIndex = m_modelView->selectionModel()->currentIndex();
    if(!selectedIndex.isValid())
    {
        return;
    }

    qDebug() << objectName() + "::editList attempting to edit (ROW: " + QString::number(selectedIndex.row());

    auto recipeActual = m_listModel->data(selectedIndex, Qt::UserRole).toList();

    auto mainLayout = new QVBoxLayout;
    auto unitTypeCombo = new QComboBox(this);
    unitTypeCombo->setEnabled(false);

    auto listName = new QLineEdit(this);
    auto ListNameActual = m_listModel->data(selectedIndex, Qt::DisplayRole).toString();
    listName->setText(ListNameActual);

    auto entryName = new QLineEdit(this);
    auto entryQuantity = new QLineEdit(this);

    auto volumeButton = new QRadioButton("Volume");
    auto weightButton = new QRadioButton("Weight");
    auto buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(volumeButton, 0);
    buttonGroup->addButton(weightButton, 1);

    connect(buttonGroup, QOverload<int, bool>::of(&QButtonGroup::buttonToggled),
            [&](int id, bool checked)
            {
                unitTypeCombo->clear();
                (id == 0) ? unitTypeCombo->addItems(utility::getVolumeTypes()) : unitTypeCombo->addItems(utility::getWeightTypes());
                unitTypeCombo->setEnabled(true);
            });

    auto typeLayout = new QHBoxLayout(this);
    typeLayout->addWidget(volumeButton);
    typeLayout->addWidget(weightButton);

    mainLayout->addLayout(typeLayout);


    auto getItemLayout = new QFormLayout;

    getItemLayout->addRow("List Name", listName);
    getItemLayout->addRow("Item Name", entryName);
    getItemLayout->addRow("Item Quantity", entryQuantity);
    getItemLayout->addRow("Unit Type", unitTypeCombo);
    mainLayout->addLayout(getItemLayout);


    auto buttonLayout = new QHBoxLayout(this);

    auto confirmButton = new QPushButton("Confirm", this);
    confirmButton->setEnabled(true);

    auto addButton = new QPushButton("Add", this);
    connect(entryQuantity, &QLineEdit::textChanged, [&](){addButton->setEnabled(true);});
    addButton->setEnabled(false);

    auto cancelButton = new QPushButton("Cancel", this);
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(confirmButton);
    mainLayout->addLayout(buttonLayout);


    auto tableModel = new QTableWidget(this);
    tableModel->setColumnCount(3);
    auto header0 = new QTableWidgetItem("Name");
    auto header1 = new QTableWidgetItem("Quantity");
    auto header2 = new QTableWidgetItem("Units");
    tableModel->setHorizontalHeaderItem(0, header0);
    tableModel->setHorizontalHeaderItem(1, header1);
    tableModel->setHorizontalHeaderItem(2, header2);

    mainLayout->addWidget(tableModel);

    // Populates the tableModel with the recipe values
    int row;
    int col;
    // Move the QVariantList up here to add the recipe values
    QVariantList result;
    result.append(listName->text());
    tableModel->setRowCount(recipeActual.length()/tableModel->columnCount());
    int itemNum = 0;
    // Pop the first one off since its the recipe name
    recipeActual.pop_front();
    result.append(recipeActual);
    for(row = 0; row < tableModel->rowCount(); row++)
        for(col = 0; col < tableModel->columnCount(); col++)
        {

            auto newItem = new QTableWidgetItem(recipeActual.value(itemNum).toString());
            tableModel->setItem(row, col, newItem);
            itemNum++;
        }


    QDialog dialog;
    dialog.setLayout(mainLayout);
    connect(addButton, &QPushButton::pressed, [&]()
    {

        auto quantity=entryQuantity->text().toDouble();
        auto type=unitTypeCombo->currentText();

        result.append(entryName->text());


        double convertedQuantity=0.0;

        if(type == "Grams")
            convertedQuantity = weightLookup(utility::WEIGHT_TYPE::GRAM, utility::WEIGHT_TYPE::KILOGRAM, quantity);
        else if(type == "Ounces")
            convertedQuantity = weightLookup(utility::WEIGHT_TYPE::OUNCE, utility::WEIGHT_TYPE::KILOGRAM, quantity);
        else if(type == "Pounds")
            convertedQuantity = weightLookup(utility::WEIGHT_TYPE::POUND, utility::WEIGHT_TYPE::KILOGRAM, quantity);
        else
            convertedQuantity = quantity;
        result.append(convertedQuantity);
        result.append(unitTypeCombo->currentText());

        qDebug() << result.length();
        QString  convertedQuantityString= QString::number(convertedQuantity);
        auto newItemName = new QTableWidgetItem(entryName->text());
        auto newItemQuantity = new QTableWidgetItem(convertedQuantityString);
        qDebug() << convertedQuantity;

        auto newItemUnits = new QTableWidgetItem(unitTypeCombo->currentText());

        int row = tableModel->rowCount();
        tableModel->insertRow(row);
        tableModel->setItem(row, 0, newItemName);
        tableModel->setItem(row, 1, newItemQuantity);
        tableModel->setItem(row, 2, newItemUnits);
        // tableModel->setCellWidget(row, 2, new QComboBox());
        entryName->clear();
        entryQuantity->clear();
        confirmButton->setEnabled(true);
    }
    );
    connect(confirmButton, &QPushButton::pressed, &dialog, &QDialog::accept);
    connect(cancelButton, &QPushButton::pressed, &dialog, &QDialog::reject);

    dialog.exec();
    if(dialog.result() == 1)
    {
        qDebug() << result;
        ListManager::removeItem();
        m_listModel->processAddItem(result);
        m_editListButton->setDisabled(false);
    }
}


}
}
