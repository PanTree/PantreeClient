﻿//
// Created by Stephen Hayes on 9/28/18.
//

#pragma once

#include <QWidget>
#include <QVariantMap>

#include "src/core/enums/Enums.h"

namespace core
{

namespace manager
{

class BaseManager : public QWidget
{
Q_OBJECT
Q_DISABLE_COPY(BaseManager)

public:

    BaseManager(QWidget *parent);
    void init();

public slots:

signals:

    void logSignal(const QString &objectName, const QString &msg, const core::LogPriority &priority) const;

protected:

    virtual void setData() = 0;
    virtual void setUI() = 0;
    virtual void checkData();
    QVariantMap m_configMap;

protected slots:

private:

    void parseConfig();

private slots:

};

}
}
