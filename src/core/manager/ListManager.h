﻿//
// Created by Tej Dangol on 10/24/18.
//

#pragma once

#include "src/core/manager/BaseManager.h"

class QComboBox;
class QFormLayout;
class QHBoxLayout;
class QItemSelection;
class QLabel;
class QLineEdit;
class QPlainTextEdit;
class QPushButton;
class QTableView;
class QToolButton;
class QVBoxLayout;

namespace core
{
namespace model
{
class ListModel;
}
namespace manager
{

class ListManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(ListManager)

public:

    ListManager(const QString &username, QWidget *parent);

signals:

    void mockRecipeSignal(QVariantList &recipe);
    void getRecipeByNameSignal(const QString &name, QVariantList &data);
    void getRecipeSignal(QVariantList &data);
    void checkInventorySignal(QVariantList &data);

protected:

    void setData() override {}
    void setUI() override;

private:

    void addItem();
    void clearData();
    void editList();
    void generateList();
    void removeItem();
    void setModel();

    core::model::ListModel *m_listModel;
    QString m_userName;

    QComboBox *m_typeCombo;
    QFormLayout *m_buttonLayout;
    QHBoxLayout *m_centralLayout;
    QHBoxLayout *m_topLayout;
    QLabel *m_listTitle;
    QLineEdit *m_entryName;
    QLineEdit *m_entryQuantity;
    QPlainTextEdit *m_listDisplay;
    QTableView *m_modelView;
    QToolButton *m_addItemButton;
    QToolButton *m_removeItemButton;
    QToolButton *m_clearDataButton;
    QToolButton *m_generateListButton;
    QToolButton *m_editListButton;
    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_bottomLayout;

private slots:

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

};

}
}
