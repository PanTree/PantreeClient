﻿//
// Created by Stephen Hayes on 10/4/18.
//

#include "src/core/manager/NetworkManager.h"
#include "src/core/networking/TcpClient.h"

#include <QtWidgets>

namespace core
{
namespace manager
{

NetworkManager::NetworkManager(const QString &username, QWidget *parent)
        : BaseManager(parent),
          m_userName(username),
          m_tcpClient(new networking::TcpClient(this)),
          m_mainLayout(new QVBoxLayout(this))

{
    setUI();
}

void NetworkManager::setUI()
{
    m_mainLayout->addWidget(m_tcpClient);
}

}
}
