﻿//
// Created by Stephen Hayes on 10/4/18.
//

#pragma once

#include "src/core/manager/BaseManager.h"

class QTextEdit;
class QVBoxLayout;

namespace core
{
namespace networking
{
class TcpClient;
}
namespace manager
{
class NetworkManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(NetworkManager)

public:

    explicit NetworkManager(const QString &username, QWidget *parent);

public slots:

protected:

    void setUI() override;
    void setData() override {}

protected slots:

private:

    QString m_userName;
    networking::TcpClient *m_tcpClient;
    QVBoxLayout *m_mainLayout;

private slots:

};

}
}
