﻿//
// Created by Cedrik Acosta
//

#pragma once

#include "src/core/manager/BaseManager.h"

class QButtonGroup;
class QFormLayout;
class QGroupBox;
class QLabel;
class QRadioButton;
class QVBoxLayout;

namespace core
{
namespace model
{
class OptionModel;
}
namespace manager
{

class OptionManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(OptionManager)

    public:

        OptionManager(const QString &username, QWidget *parent);

    protected:

        void setData() override {}
        void setUI() override;

    private:

        void setConfig();
        bool testOnline();

        QString m_userName;
        QRadioButton *m_addOptionButton;
        QLabel *m_addLabel;
        QButtonGroup *m_buttonGroup;
        QVBoxLayout *m_mainLayout;

        QGroupBox *m_addGroupBox;

};
}
}
