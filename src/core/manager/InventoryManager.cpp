﻿//
// Created by Stephen Hayes on 9/29/18.
//

#include "src/core/parser/ConversionHelper.h"
#include "src/core/manager/InventoryManager.h"
#include "src/core/model/InventoryModel.h"
#include <QtWidgets>
#include <QItemSelectionModel>

namespace core
{
namespace manager
{

InventoryManager::InventoryManager(const QString &username, QWidget *parent)
    : BaseManager(parent),
      m_inventoryModel(new core::model::InventoryModel(username, this)),
      m_userName(username),
      m_modelView(new QTableView(this)),
      m_mainLayout(new QVBoxLayout(this))
{
    setUI();
    setModel();
}

void InventoryManager::setUI()
{
    qDebug() << "setUI() begin.";
    m_addItemButton=new QToolButton(this);
    m_addItemButton->setIcon(QIcon(":/icons/Add.png"));
    m_removeItemButton=new QToolButton(this);
    m_removeItemButton->setIcon(QIcon(":/icons/Delete.png"));
    m_clearDataButton=new QToolButton(this);
    m_clearDataButton->setIcon(QIcon(":/icons/Abort.png"));

    m_typeCombo = new QComboBox(this);
    m_entryName = new QLineEdit(this);
    m_entryQuantity = new QLineEdit(this);
    auto volumeButton = new QRadioButton("Volume");
    auto weightButton = new QRadioButton("Weight");
    auto buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(volumeButton, 0);
    buttonGroup->addButton(weightButton, 1);

    connect(buttonGroup, QOverload<int, bool>::of(&QButtonGroup::buttonToggled),
            [&](int id, bool checked)
    {
        m_typeCombo->clear();
        (id == 0) ? m_typeCombo->addItems(utility::getVolumeTypes()) : m_typeCombo->addItems(utility::getWeightTypes());
        m_typeCombo->setEnabled(true);
    });

    auto typeLayout = new QHBoxLayout(this);
    typeLayout->addWidget(volumeButton);
    typeLayout->addWidget(weightButton);

    m_mainLayout->addLayout(typeLayout);
    m_topLayout = new QHBoxLayout;
    m_topLayout->addWidget(m_entryName);
    m_topLayout->addWidget(m_entryQuantity);
    m_topLayout->addWidget(m_typeCombo);

    auto enterLabel = new QLabel("Add Item");
    enterLabel->setAlignment(Qt::AlignCenter);
    m_mainLayout->addWidget(enterLabel);

    m_mainLayout->addLayout(m_topLayout);

    m_centralLayout = new QHBoxLayout;
    m_buttonLayout = new QFormLayout;
    m_centralLayout->addLayout(m_buttonLayout);
    m_centralLayout->addWidget(m_modelView);
    m_mainLayout->addLayout(m_centralLayout);

    m_buttonLayout->addRow("Add Item:", m_addItemButton);
    m_buttonLayout->addRow("Remove Item:", m_removeItemButton);
    m_buttonLayout->addRow("Clear Inventory:", m_clearDataButton);

    connect(m_addItemButton, &QAbstractButton::pressed, this, &InventoryManager::addItem);
    connect(m_removeItemButton, &QAbstractButton::pressed, this, &InventoryManager::removeItem);
    connect(m_clearDataButton, &QAbstractButton::pressed, this, &InventoryManager::clearData);
}

void InventoryManager::checkInventorySlot(QVariantList &data)
{
    m_inventoryModel->checkIfContains(data);
}

void InventoryManager::addItem()
{
    auto name = m_entryName->text();
    auto quantity = m_entryQuantity->text().toDouble();
    auto type = m_typeCombo->currentText();
    QVariantList i{name, quantity, type};
    m_inventoryModel->processAddItem(i);
}

void InventoryManager::setModel()
{
    connect(m_inventoryModel, &model::BaseModel::logSignal, this, &BaseManager::logSignal);
    m_inventoryModel->init();
    m_modelView->setModel(m_inventoryModel);
    m_modelView->setSelectionMode(QAbstractItemView::SingleSelection);
}

void InventoryManager::removeItem()
{
    auto selectedIndex = m_modelView->selectionModel()->currentIndex();
    qDebug() << "removeItem attempting to remove (ROW): " + QString::number(selectedIndex.row());
    m_inventoryModel->processRemoveItem(selectedIndex);
}

void InventoryManager::clearData()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::critical(this, tr("QMessageBox::critical()"),
                                  QString("Are you sure you want to clear all the data?"),
                                  QMessageBox::Cancel | QMessageBox::Ok);
    if (reply == QMessageBox::Cancel)
        return;

    m_inventoryModel->processClearData();
}

}
}
