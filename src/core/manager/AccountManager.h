﻿//
// Created by Stephen Hayes on 10/12/18.
//

#pragma once
#include "src/core/manager/BaseManager.h"

class QItemSelectionModel;
class QPushButton;
class QTableView;
class QVBoxLayout;

namespace core
{
namespace account
{
class CreateUserDialog;
class LoginDialog;
}
namespace model
{
class UserModel;
}
namespace manager
{
class AccountManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(AccountManager)

public:

    AccountManager(QWidget *parent);

public slots:

    void loggedInSlot();
    void loggedOutSlot();

signals:

    void loginSignal(const QString &username);
    void logoutSignal();

protected:

    void setData() override {}
    void setUI() override;

private:

    void setModel();
    void setConnections();

    account::CreateUserDialog *m_createUserDialog;
    account::LoginDialog *m_loginDialog;
    model::UserModel *m_userModel;

    QPushButton *m_createUserButton;
    QPushButton *m_loginButton;
    QPushButton *m_logoutButton;
    QTableView *m_modelView;
    QVBoxLayout *m_mainLayout;

private slots:

    void createUser();
    void login();
    void tryCreateUserSlot(const QVariantList &data);
    void tryLoginSlot(const QString &username, const QString &password);

};

}
}
