﻿//
// Created by Stephen Hayes on 9/26/18.
//

#pragma once

#include <QMainWindow>
#include <QMap>

#include "src/core/enums/Enums.h"

class QActionGroup;
class QComboBox;
class QHBoxLayout;
class QLabel;
class QStackedWidget;
class QTextEdit;

namespace core
{
namespace manager
{
class AccountManager;
class InventoryManager;
class NetworkManager;
class ListManager;
class RecipeManager;
class OptionManager;
}

class MainWindow : public QMainWindow
{
Q_OBJECT
Q_DISABLE_COPY(MainWindow)

public:

    explicit MainWindow(QWidget *parent = Q_NULLPTR);

public slots:

    void logSlot(const QString &string, const QString &msg, const LogPriority &priority = LogPriority::Normal) const;
    void loginSlot(const QString &username);
    void logoutSlot();

signals:

    void loggedInSignal();
    void loggedOutSignal();

protected:
private:

    void setMenu();
    void setToolBar();
    void setLog();

    void initModules();
    void initAccountManager();
    bool initNetworkManager();
    bool initInventoryManager();
    bool initListManager();
    bool initRecipeManager();
    bool initOptionManager();

    manager::AccountManager *m_accountManager;
    manager::InventoryManager *m_inventoryManager;
    manager::ListManager *m_listManager;
    manager::NetworkManager *m_networkManager;
    manager::RecipeManager *m_recipeManager;
    manager::OptionManager *m_optionManager;

    bool m_loggedIn = false;
    QString m_userName;

    QActionGroup *m_userActionGroup;
    QComboBox *m_widgetComboBox;
    QLabel *m_activeUser;
    QStackedWidget *m_centralWidget;
    QTextEdit *m_configLog;
    QToolBar *m_toolBar;

private slots:

    void about();

};

}
