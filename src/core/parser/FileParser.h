﻿//
// Created by Stephen Hayes on 9/28/18.
//

#pragma once

#include <QVariantList>
#include <QVariantMap>

namespace core
{
namespace utility
{

class FileParser
{

public:

    FileParser() = delete;
    FileParser(const FileParser&) = delete;
    FileParser& operator=( const FileParser& ) = delete;

    static QVariantMap parseJson(const QString &filename);
    static QVector<QVariantList> readTable(const QString &filename);
    static void writeTable(const QString &filename, const QVector<QVariantList> &data);
    static void writeJson(const QVariantMap &data, const QString &path);
};

}
}

