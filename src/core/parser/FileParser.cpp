﻿//
// Created by Stephen Hayes on 9/28/18.
//

#include "FileParser.h"
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDataStream>

namespace core
{
namespace utility
{

QVariantMap FileParser::parseJson(const QString &filename)
{
    qDebug() << "beginning.";
    QFile configFile(filename);
    if(!configFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "not detected.";
        return QVariantMap({{"error", true}});
    }
    qDebug() << "file open.";

    QByteArray configData=configFile.readAll();
    QJsonDocument loadConfig(QJsonDocument::fromJson(configData));
    QJsonObject data=loadConfig.object();
    qDebug() << "JsonObject loaded, size: " + QString::number(data.size());
    configFile.close();
    return data.toVariantMap();
}

void FileParser::writeTable(const QString &filename, const QVector<QVariantList> &data)
{
    qDebug() << "begin.";
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_0);
    out << data;
    file.close();
}
void FileParser::writeJson(const QVariantMap &data, const QString &path)
{
    qDebug() << "begin.";

    QFile file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qWarning("Couldn't open file.");
        return;
    }
    qDebug() << data;
    QJsonObject dataObject = QJsonObject::fromVariantMap(data);
    QJsonDocument jDoc(dataObject);
    file.write(jDoc.toJson());
    file.close();
    qDebug() << "file written.";
}

QVector<QVariantList> FileParser::readTable(const QString &filename)
{
    QVector<QVariantList> data;
    QFile datafile(filename);
    if(!datafile.open(QIODevice::ReadOnly))
    {
        qWarning() << "not detected.";
        return data;
    }
    qDebug() << "file open.";

    QDataStream in(&datafile);
    in.setVersion(QDataStream::Qt_5_0);
    in >> data;
    datafile.close();
    return data;
}
}
}
