﻿//
// Created by Stephen Hayes on 10/28/18.
//
#pragma once

#include "src/core/enums/Enums.h"
#include <QObject>
#include <utility> /// for std::pair

namespace core{
namespace utility{

enum class VOLUME_TYPE : char
{
    ML,
    LITER,
    FL_OUNCE,
    PINT,
    CUP,
    QUART,
    GALLON,
    TBSP,
    TSP
};

enum class WEIGHT_TYPE : char
{
    GRAM,
    KILOGRAM,
    OUNCE,
    POUND,
};

static constexpr std::pair<VOLUME_TYPE, double> volumeMap[] =
{
    {VOLUME_TYPE::TBSP, 1},
    {VOLUME_TYPE::LITER, .015},
    {VOLUME_TYPE::FL_OUNCE, .5},
    {VOLUME_TYPE::PINT, .03125},
    {VOLUME_TYPE::CUP, .0625},
    {VOLUME_TYPE::QUART, .015625},
    {VOLUME_TYPE::GALLON, .0039},
    {VOLUME_TYPE::ML, 15},
    {VOLUME_TYPE::TSP, 3},
};

static constexpr std::pair<WEIGHT_TYPE, double> weightMap[] =
{
    {WEIGHT_TYPE::GRAM, 1},
    {WEIGHT_TYPE::KILOGRAM, .001},
    {WEIGHT_TYPE::OUNCE, .0357},
    {WEIGHT_TYPE::POUND, .0022},
};

static constexpr std::array<const char*, 9> VOLUME_LABELS{"Mililiter", "Liter", "Fluid Ounce", "Pint"
                                                         "Cup", "Quart", "Gallon", "Tablespoon", "Teaspoon"};

static constexpr std::array<const char*, 4> WEIGHT_LABELS{"Gram", "Kilogram", "Ounce", "Pound"};


static QStringList getVolumeTypes()
{
    QStringList volumeTypes;
    for(const auto &it : VOLUME_LABELS)
    {
        volumeTypes.append(it);
    }
    return volumeTypes;
}

static QStringList getWeightTypes()
{
    QStringList weightTypes;
    for(const auto &it : WEIGHT_LABELS)
    {
        weightTypes.append(it);
    }
    return weightTypes;
}

static const char* weightLabelLookup(WEIGHT_TYPE w)
{
    const std::map<WEIGHT_TYPE, const char*> weightStrings
    {
        {WEIGHT_TYPE::GRAM, WEIGHT_LABELS[0]},
        {WEIGHT_TYPE::KILOGRAM, WEIGHT_LABELS[1]},
        {WEIGHT_TYPE::OUNCE, WEIGHT_LABELS[2]},
        {WEIGHT_TYPE::POUND, WEIGHT_LABELS[3]},
    };

    auto it  = weightStrings.find(w);
    return it == weightStrings.end() ? "fail" : it->second;
}

static const char* volumeLabelLookup(VOLUME_TYPE v)
{
    const std::map<VOLUME_TYPE, const char*> volumeStrings
    {
        {VOLUME_TYPE::TBSP, "Mililiters"},
        {VOLUME_TYPE::LITER, "Liters"},
        {VOLUME_TYPE::FL_OUNCE, "Fluid Ounces"},
        {VOLUME_TYPE::PINT, "Pints"},
        {VOLUME_TYPE::CUP, "Cups"},
        {VOLUME_TYPE::QUART, "Quarts"},
        {VOLUME_TYPE::GALLON, "Gallons"},
        {VOLUME_TYPE::ML, "Tablespoons"},
        {VOLUME_TYPE::TSP, "Teaspoons"},
    };

    auto it  = volumeStrings.find(v);
    return it == volumeStrings.end() ? "fail" : it->second;
}

static constexpr double volumeLookup(VOLUME_TYPE initial, VOLUME_TYPE target, double quantity)
{
    double initialRatio{0};
    double targetRatio{0};
    for (int i = 0; i<9; ++i)
    {
        if(volumeMap[i].first == target) targetRatio = volumeMap[i].second;
        else if(volumeMap[i].first == initial) initialRatio = volumeMap[i].second;
    }
    return (quantity / initialRatio) * targetRatio;
}

static constexpr double weightLookup(WEIGHT_TYPE initial, WEIGHT_TYPE target, double quantity)
{
    double initialRatio{0};
    double targetRatio{0};
    for (int i = 0; i<4; ++i)
    {
        if(weightMap[i].first == target) targetRatio = weightMap[i].second;
        else if(weightMap[i].first == initial) initialRatio = weightMap[i].second;
    }
    return (quantity / initialRatio) * targetRatio;
}

}
}
