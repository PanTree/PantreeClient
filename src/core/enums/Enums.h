﻿//
// Created by Stephen Hayes on 10/5/18.
//
#pragma once

namespace core
{

enum class LogPriority : int
{
    Normal = 0,
    Warning = 1,
    Critical = 2,
    PositiveAlert = 3,
};

}
