//
// Created by Stephen Hayes on 9/28/18.
//
#include <QtWidgets>
#include <QDebug>

#include "src/core/manager/SchemaManager.h"
#include "src/core/parser/JsonParser.h"

namespace core
{
namespace manager
{

SchemaManager::SchemaManager(QWidget *parent)
        : BaseManager(parent),
          m_mainLayout(new QHBoxLayout(this)),
          m_mapView(new QPlainTextEdit(this)),
          m_fileDialog(new QFileDialog(this)),
          m_loadedFile(new QLabel(this))
{
    m_fileDialog->setFileMode(QFileDialog::AnyFile);
    m_fileDialog->setNameFilter(tr("Configuration Data (*.json)"));
    m_fileDialog->setViewMode(QFileDialog::Detail);
    setUI();
}

void SchemaManager::setUI()
{
    m_addAttribute=new QPushButton("Add Attribute", this);
    m_removeAttribute=new QPushButton("Remove Attribute", this);
    m_clearData=new QPushButton("Clear Data", this);
    m_loadData=new QPushButton("Load Data", this);
    m_writeData=new QPushButton("Write Data", this);

    connect(m_addAttribute, &QAbstractButton::pressed, this, &SchemaManager::addAttribute);
    connect(m_removeAttribute, &QAbstractButton::pressed, this, &SchemaManager::removeAttribute);
    connect(m_clearData, &QAbstractButton::pressed, this, &SchemaManager::clearData);
    connect(m_loadData, &QAbstractButton::pressed, this, &SchemaManager::loadData);
    connect(m_writeData, &QAbstractButton::pressed, this, &SchemaManager::writeData);

    auto buttonLayout=new QFormLayout;
    m_mainLayout->addLayout(buttonLayout);

    buttonLayout->addRow("Add an attribute (key/value)", m_addAttribute);
    buttonLayout->addRow("Remove an attribute", m_removeAttribute);
    buttonLayout->addRow("Clear existing data", m_clearData);
    buttonLayout->addRow("Load data from file", m_loadData);
    buttonLayout->addRow("Write data to file", m_writeData);

    m_mainLayout->addWidget(m_mapView);
}

void SchemaManager::addAttribute()
{
    bool ok;
    QString key=QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                      tr("Key:"), QLineEdit::Normal,
                                      QDir::home().dirName(), &ok);
    QString value=QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                        tr("Value:"), QLineEdit::Normal,
                                        QDir::home().dirName(), &ok);
    if(ok && !key.isEmpty() && !value.isEmpty())
    {
        m_attributeMap.insert(key, value);
        refreshMapView();
    }
}

void SchemaManager::removeAttribute()
{
    QStringList itemList(m_attributeMap.keys());
    bool ok;
    QString item = QInputDialog::getItem(this, tr("QInputDialog::getItem()"),
                                         tr("Keys:"), itemList, 0, false, &ok);
    if (ok && !item.isEmpty())
    {
        m_attributeMap.remove(item);
        refreshMapView();
    }
}

void SchemaManager::clearData()
{
    m_attributeMap.clear();
    refreshMapView();
}

void SchemaManager::loadData()
{
    m_mapView->clear();
    QString filename;
    if(m_fileDialog->exec())
    {
        filename=m_fileDialog->selectedFiles().at(0);
        m_mapView->clear();
        m_mapView->appendPlainText(filename);
        m_attributeMap=parser::JsonParser::parseJson(filename);
        refreshMapView();
    }
}

void SchemaManager::refreshMapView()
{
    m_mapView->clear();
    QMapIterator<QString, QVariant> i(m_attributeMap);
    while(i.hasNext())
    {
        i.next();
        m_mapView->appendPlainText(i.key() + "::" + i.value().toString());
    }
}

void SchemaManager::writeData()
{
    bool ok;
    QString path = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                         tr("File Name:"), QLineEdit::Normal,
                                         QDir::home().dirName(), &ok);
    if (ok && !path.isEmpty())
        parser::JsonParser::writeJson(m_attributeMap, path);
}


}
}