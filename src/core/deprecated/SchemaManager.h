﻿//
// Created by Stephen Hayes on 9/28/18.
//

#pragma once
#include "BaseManager.h"

class QFileDialog;
class QHBoxLayout;
class QLabel;
class QPlainTextEdit;
class QPushButton;

namespace core{
namespace manager{


class SchemaManager : public BaseManager
{
Q_OBJECT
Q_DISABLE_COPY(SchemaManager)

public:

    explicit SchemaManager(QWidget *parent);

public slots:
signals:
protected:

    void setUI() override;
    void setData() override {}

protected slots:
private:

    void addAttribute();
    void removeAttribute();
    void clearData();
    void loadData();
    void writeData();
    void refreshMapView();

    QPushButton *m_addAttribute;
    QPushButton *m_removeAttribute;
    QPushButton *m_clearData;
    QPushButton *m_writeData;
    QPushButton *m_loadData;

    QVariantMap m_attributeMap;
    QHBoxLayout *m_mainLayout;

    QPlainTextEdit *m_mapView;
    QFileDialog *m_fileDialog;
    QLabel *m_loadedFile;

private slots:


};

}
}
