﻿//
// Created by Stephen Hayes on 11/31/18.
//

#include "src/core/data/BaseData.h"
namespace core
{
namespace data
{

BaseData::BaseData(const QString &name, double quantity, QObject *parent)
    :
      QObject(parent),
      m_name(name),
      m_quantity(quantity)
{
    setObjectName("BaseData");
}

}
}
