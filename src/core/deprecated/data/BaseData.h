﻿//
// Created by Stephen Hayes on 11/1/18.
//

#pragma once

#include <QObject>

namespace core
{
namespace data
{
class BaseData : public QObject
{
Q_OBJECT
Q_DISABLE_COPY(BaseData)

public:

    BaseData(const QString &name, double quantity, QObject *parent);

    const QString &getName() const {return m_name;}
    double getQuantity() const {return m_quantity;}

public slots:

signals:

protected:
protected slots:
private:

    QString m_name;
    double m_quantity;

private slots:

};

}
}

